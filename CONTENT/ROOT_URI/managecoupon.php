<script>
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
</script>
<?php if (!$_SESSION['LoggedIn']){
 	header("Location: signIn");
 }
?>

<?php if($_SESSION['LoggedIn']): ?>

<div class="container-fluid" id="vue">
  <div id="fb-root"></div>
	
	 <div class="row">
        <div class="col-md-12">
          <h1 class="display-7 text-center">Coupon Management</h1>

          <!-- ---------------USER DETAILS TABLE ------------- -->

          <div class="row">
          <div class="col-12 ml-auto mr-auto">
          	<div class="ml-auto text-center">
          		<div class="btn-group">
          			<button class="btn btn-info" data-toggle="modal" data-target="#modal2">Add a new coupon</button>
          		</div>
            </div><br>
            <div class="ml-auto">
              <button class="btn btn-outline-danger " onclick="exportTableToCSV('sparshPartners.csv')">Export Data To CSV File</button>
            </div><br>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Existing Coupons</h3>
                <div class="card-tools">
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               <div class=" table-responsive p-0">
                <table class="display table table-hover text-nowrap" id="couponList">
                  <thead>
			            <tr>
		                <th>ID</th>
		                <th>Coupon Name</th>
                    <th>Coupon Code</th>
		                <th>Discount(%)</th>
                    <th>Domain</th>
		                <th>Start Date</th>
                    <th>End Date</th>
                    <th>Action</th>
			            </tr>
			        </thead>
			        <tbody id="couponListBody">
			        	
			        </tbody>
			        <tfoot>
			            <tr>
		                <th>ID</th>
                    <th>Coupon Name</th>
                    <th>Coupon Code</th>
                    <th>Discount(%)</th>
                    <th>Domain</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Action</th>
			            </tr>
			        </tfoot>
                </table>
              </div>

              </div> 
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
          

        </div>
      </div>
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Update Coupon</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               <div class="alert alert-warning mx-auto" id="formWarning2" style="display: none;"></div>
                <div class="alert alert-success mx-auto" id="formSuccess2" style="display: none;"></div>
              <form class="form" method="POST">
                <div class="col-md-8 col-sm-12 mx-auto">
                  <div class="row">
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label>Coupon Name</label>
                        <input type="text" class="form-control" readonly placeholder="Coupon Name" id="editCouponName" required>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label>Coupon Code</label>
                        <input type="text" class="form-control" readonly placeholder="Unique Code" id="editCouponCode" required>
                      </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                      <div class="form-group">
                        <label>Domain</label>
                        <input type="text" class="form-control" id="editDomain" readonly required>
                      </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                      <div class="form-group">
                        <label>Product</label>
                        <textarea class="form-control" readonly cols="3" rows="4" id="editProduct"></textarea>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label>Validity Start</label>
                        <input type="date" class="form-control" id="editStartDate" required>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label>Validity End</label>
                        <input type="date" class="form-control" id="editEndDate" required>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12 mx-auto">
                      <div class="form-group">
                        <label>Applied Discount(%)</label>
                        <input type="number" class="form-control" placeholder="Eg:- 10" id="editDiscount" required>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <p id="saveMsg2" style="display: none; font-weight: bold;">Saving Data...</p>
              <button id="saveBtn2" type="button" onclick="saveCouponDetails();" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>
</div>

<!-- Add Coupon modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Coupon</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning mx-auto" id="formWarning" style="display: none;"></div>
        <div class="alert alert-success mx-auto" id="formSuccess" style="display: none;"></div>
        <form class="form" method="POST">
        	<div class="col-md-12 col-sm-12 mx-auto">
        		<div class="row">
        			<div class="col-md-6 col-sm-12">
        				<div class="form-group">
                  <label>Coupon Name</label>
        					<input type="text" class="form-control" placeholder="Coupon Name" id="couponName" required>
        				</div>
        			</div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <label>Coupon Code</label>
                  <input type="text" class="form-control" placeholder="Unique Code" id="couponCode" required>
                </div>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="form-group">
                  <label>Domain</label>
                  <select class="form-control" id="domain" onchange="selectDomain()">
                    <option value="">Select Domain</option>
                    <option value="atheneumglobal.education">atheneumglobal.education</option>
                    <option value="teachertrainingbangalore.in">teachertrainingbangalore.in</option>
                    <option value="atheneumglobal.co.uk">atheneumglobal.co.uk</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12 col-sm-12" id="programOptions">
                
              </div>
        			<div class="col-md-6 col-sm-12">
        				<div class="form-group">
                  <label>Validity Start</label>
        					<input type="date" class="form-control" id="startDate" required>
        				</div>
        			</div>
        			<div class="col-md-6 col-sm-12">
        				<div class="form-group">
                  <label>Validity End</label>
        					<input type="date" class="form-control" id="endDate" required>
        				</div>
        			</div>
              <div class="col-md-6 col-sm-12 mx-auto">
                <div class="form-group">
                  <label>Applied Discount(%)</label>
                  <input type="number" class="form-control" placeholder="Eg:- 10" id="discount" required>
                </div>
              </div>
        		</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <p id="saveMsg" style="display: none; font-weight: bold;">Saving Data...</p><button type="button" id="saveBtn" onclick="addCoupon();" class="btn btn-primary">Save changes</button> 
      </div>
    </div>
  </div>
</div>

<!-- Vanilla js & Jquery code -->
<script type="text/javascript">
  alert = function() {};
	var couponName = null;
	var couponCode = null;
	var startDate = null;
	var endDate = null;
	var discount = null;
  var domain = null;
  var product = [];
	var formWarning = document.getElementById('formWarning');
	var formSuccess = document.getElementById('formSuccess');
	var saveMsg = document.getElementById('saveMsg');
	var saveBtn = document.getElementById('saveBtn');

  function selectDomain(){
    var e = document.getElementById("domain");
    domain = e.options[e.selectedIndex].value;
    setPrograms(domain);
  }

  function selectAll(ele){
    var checkboxes = document.querySelectorAll('input[type=checkbox]');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
  }

  function getPrograms(){
    var checkboxes = document.querySelectorAll('input[type=checkbox]:checked')

    for (var i = 0; i < checkboxes.length; i++) {
      if (checkboxes[i].value) {
        product.push(checkboxes[i].value)
      }
    }
  }

  function setPrograms(domainName) {
    switch(domainName){
      case 'teachertrainingbangalore.in':{
        document.getElementById('programOptions').innerHTML = bangaloreCheckBoxes;
        break;
      }
      case 'atheneumglobal.education':{
        document.getElementById('programOptions').innerHTML = atheneumGlobalCheckBoxes;
        break;
      }
      case 'atheneumglobal.co.uk':{
        document.getElementById('programOptions').innerHTML = atheneumUKCheckBoxes;
        break;
      }
      default:{

      }
    }
  }

  function fetchData() {
		$.ajax({ 
	      url: "/API/V1/?couponList",
	      dataType:"html",
	      type: "post",
	      success: function(data){
	        var table = $('#couponList');
	        var body = $('#couponListBody');
          // table.remove();
	        table.find("tbody tr").remove();
	        table.find("tbody div").remove();
	        body.append(data);
          // $("#sparshUsers").dataTable().fnDestroy();
	        $('#couponList').DataTable( {
		        "order": [[ 0, "desc" ]],
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            // "bDestroy": true
	    	});
	      }
	    });
	}
	
	$(document).ready(fetchData());

	function addCoupon(){
    getPrograms();
		saveMsg.style.display = "inline-block";
 		saveMsg.innerHTML = 'Saving Data...';
 		saveBtn.disabled = true;
 		formWarning.style.display = 'none';
 		formSuccess.style.display = 'none';
 		couponName = document.getElementById('couponName').value; 
 		couponCode = document.getElementById('couponCode').value; 
 		startDate = document.getElementById('startDate').value; 
 		endDate = document.getElementById('endDate').value; 
 		discount = document.getElementById('discount').value; 
		if (!couponName || !couponCode || !discount || !startDate || !endDate || !product || product.length == 0 || !domain) {
			formWarning.style.display = 'inline-block';
			formWarning.innerHTML = 'Please fill all the fields';
			saveMsg.style.display = "none";
			saveBtn.disabled = false;
		}else {
      // console.log(product)
      fetch('/API/V1/?checkCoupon&couponCode='+couponCode+'&couponName='+couponName+'&domain='+domain)
      .then(
        function(response) {
          if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' +
              response.status);
            return;
          }
          response.json().then(function(data) {
            if (data.data == "present") {
              formWarning.style.display = 'inline-block';
              formWarning.innerHTML = 'Coupon Code already exists';
              saveMsg.style.display = "none";
              saveBtn.disabled = false;
            }else{
              formWarning.style.display = 'none';
              let formData = new FormData();
              formData.append("formName", "addCoupon");
              formData.append("couponName", couponName);
              formData.append("couponCode", couponCode);
              formData.append("startDate", startDate);
              formData.append("endDate", endDate);
              formData.append("discount", discount);
              formData.append("domain", domain);
              formData.append("product", product);
              fetch('/API/V1/', {
                method: "POST",
                body: formData
              }).then(function(response) {
                if (response.status !== 200) {
                  console.log(
                    "Looks like there was a problem. Status Code: " + response.status
                  );
                  return;
                }
                response.json().then(function(data) {
                  console.log(data);
                  saveBtn.disabled = false;
                  saveMsg.style.display = "none";
                  if (data.errorm != null) {
                    formWarning.style.display = 'inline-block';
                    formWarning.innerHTML = data.errorm;
                  } else {
                    formWarning.style.display = 'none';
                    formSuccess.style.display = 'inline-block';
                    formSuccess.innerHTML = data.data;
                    // sendMailToPartner();
                    document.getElementById('couponName').value = null;
                    document.getElementById('couponCode').value = null;
                    document.getElementById('startDate').value = null;
                    document.getElementById('endDate').value = null;
                    document.getElementById('discount').value = null;
                    // document.getElementById('product').value = null;
                    document.getElementById('domain').selectedIndex = -1;
                    document.getElementById('programOptions').innerHTML = "";
                    fetchData();
                  }
                });
              });
            }
          })
        }
      );
    }
  }

  // EDIT VARIABLES'

  var editCouponName = null;
  var editCouponCode = null;
  var editDiscount = null;
  var editStartDate = null;
  var editEndDate = null;
  var editProduct = null;
  var editDomain = null;
  var formWarning2 = document.getElementById('formWarning2');
  var formSuccess2 = document.getElementById('formSuccess2');
  var saveMsg2 = document.getElementById('saveMsg2');
  var saveBtn2 = document.getElementById('saveBtn2');

  function fetchCouponDetails(couponCode, domain){
    fetch('/API/V1/?fetchSingleCoupon&couponCode='+couponCode+'&domain='+domain)
      .then(
        function(response) {
          if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' +
              response.status);
            return;
          }
          response.json().then(function(data) {
            if(data){
              console.log(data.data.PRODUCT.split(","));
              document.getElementById('editCouponName').value = data.data.COUPON_NAME;
              document.getElementById('editCouponCode').value = data.data.COUPON_CODE;
              document.getElementById('editDiscount').value = data.data.DISCOUNT;
              document.getElementById('editStartDate').value = data.data.START_DATE;
              document.getElementById('editEndDate').value = data.data.END_DATE;
              document.getElementById('editProduct').value = data.data.PRODUCT;
              document.getElementById('editDomain').value = data.data.DOMAIN;
            }else{
              // self.noticeAlert = "No blog present. Please create one!";
              // self.showDismissibleAlert = true;
              // self.pages = null;
            }
            
          });
        }
      )
      .catch(function(err) {
        console.log('Fetch Error :-S', err);
      });
   }

   function saveCouponDetails(){
      saveMsg2.style.display = "inline-block";
      saveMsg2.innerHTML = 'Saving Data...';
      saveBtn2.disabled = true;
      formWarning2.style.display = 'none';
      formSuccess2.style.display = 'none';
      editCouponName = document.getElementById('editCouponName').value; 
      editCouponCode = document.getElementById('editCouponCode').value; 
      editDiscount = document.getElementById('editDiscount').value; 
      editStartDate = document.getElementById('editStartDate').value; 
      editEndDate = document.getElementById('editEndDate').value; 
      editProduct = document.getElementById('editProduct').value; 
      editDomain = document.getElementById('editDomain').value; 
      // console.log(partnerCodeEdit)
      if (!editCouponCode || !editCouponName || !editDiscount || !editStartDate || !editEndDate || !editProduct) {
        formWarning2.style.display = 'inline-block';
        formWarning2.innerHTML = 'Please fill all the fields';
        saveMsg2.style.display = "none";
        saveBtn2.disabled = false;
      }else{
        formWarning2.style.display = 'none';
        let formData = new FormData();
        formData.append("formName", "updateCouponDetails");
        formData.append("couponName", editCouponName);
        formData.append("couponCode", editCouponCode);
        formData.append("startDate", editStartDate);
        formData.append("endDate", editEndDate);
        formData.append("discount", editDiscount);
        formData.append("product", editProduct);
        formData.append("domain", editDomain);
        fetch('/API/V1/', {
          method: "POST",
          body: formData
        }).then(function(response) {
          if (response.status !== 200) {
            console.log(
              "Looks like there was a problem. Status Code: " + response.status
            );
            return;
          }
          response.json().then(function(data) {
            // console.log(data);
            saveBtn2.disabled = false;
            saveMsg2.style.display = "none";
            if (data.errorm != null) {
              formWarning2.style.display = 'inline-block';
              formWarning2.innerHTML = data.errorm;
            } else {
              formWarning2.style.display = 'none';
              formSuccess2.style.display = 'inline-block';
              formSuccess2.innerHTML = data.data;
              // sendMailToPartner();
              fetchData();
              fetchCouponDetails(editCouponCode, editDomain);
            }
         });
      });
    }
   }

   function deleteCoupon(couponCode, domain) {
    // console.log(parentId)
      var result = confirm('Are you sure you want to delete the coupon?');
      if (result) {
        let formData = new FormData();
        formData.append("formName", "deleteCoupon");
        formData.append("couponCode", couponCode);
        formData.append("domain", domain);
        fetch('/API/V1/', {
            method: "POST",
            body: formData
          }).then(function(response) {
            if (response.status !== 200) {
              console.log(
                "Looks like there was a problem. Status Code: " + response.status
              );
              return;
            }
            response.json().then(function(data) {
              // console.log(data);
              fetchData();
           });
        });
      }
   }


  function makeSiteWide(couponCode, domain) {
    var result = confirm('Are you sure you want to make this coupon site wide applicable?');
    if (result) {
      let formData = new FormData();
      formData.append("formName", "makeSiteWide");
      formData.append("couponCode", couponCode);
      formData.append("domain", domain);
      fetch('/API/V1/', {
          method: "POST",
          body: formData
        }).then(function(response) {
          if (response.status !== 200) {
            console.log(
              "Looks like there was a problem. Status Code: " + response.status
            );
            return;
          }
          response.json().then(function(data) {
            // console.log(data);
            fetchData();
         });
      });
    }
  }

  function removeSiteWide(couponCode, domain) {
    var result = confirm('Are you sure you want to remove this coupon site wide applicability?');
    if (result) {
      let formData = new FormData();
      formData.append("formName", "removeSiteWide");
      formData.append("couponCode", couponCode);
      formData.append("domain", domain);
      fetch('/API/V1/', {
          method: "POST",
          body: formData
        }).then(function(response) {
          if (response.status !== 200) {
            console.log(
              "Looks like there was a problem. Status Code: " + response.status
            );
            return;
          }
          response.json().then(function(data) {
            // console.log(data);
            fetchData();
         });
      });
    }
  }
  const atheneumGlobalCheckBoxes =
   '<div class="form-group"><label>Products</label><br><input type="checkbox" onchange="selectAll(this);" value=""><label>Select All</label><br><input type="checkbox" value="Certification Course-Montessori Studies"><label>Certification Course-Montessori Studies</label><br><input type="checkbox" value="Graduate Course-Montessori Studies"><label>Graduate Course-Montessori Studies</label><br><input type="checkbox" value="Post Graduate Course-Montessori Studies"><label>Post Graduate Course-Montessori Studies</label><br><input type="checkbox" value="Certification Course-Pre and Primary Education">      <label>Certification Course-Pre and Primary Education</label><br>      <input type="checkbox" value="Graduate Course-Pre and Primary Education">      <label>Graduate Course-Pre and Primary Education</label><br>      <input type="checkbox" value="Post Graduate Course-Pre and Primary Education">      <label>Post Graduate Course-Pre and Primary Education</label>      <br>      <input type="checkbox" value="Certification Course-Early Childhood Education and Care">      <label>Certification Course-Early Childhood Education and Care</label><br>      <input type="checkbox" value="Graduate Course-Early Childhood Education and Care">      <label>Graduate Course-Early Childhood Education and Care</label><br>      <input type="checkbox" value="Post Graduate Course-Early Childhood Education and Care">      <label>Post Graduate Course-Early Childhood Education and Care</label><br>      <input type="checkbox" value="Certification Course-Nursery Teacher Training">      <label>Certification Course-Nursery Teacher Training</label><br>      <input type="checkbox" value="Graduate Course-Nursery Teacher Training">      <label>Graduate Course-Nursery Teacher Training</label><br>      <input type="checkbox" value="Post Graduate Course-Nursery Teacher Training">      <label>Post Graduate Course-Nursery Teacher Training</label><br>      <input type="checkbox" value="Certification Course-School Organization : Administration and Management">      <label>Certification Course-School Organization : Administration and Management</label><br>      <input type="checkbox" value="Graduate Course-School Organization : Administration and Management">      <label>Graduate Course-School Organization : Administration and Management</label><br>      <input type="checkbox" value="Post Graduate Course-School Organization : Administration and Management">      <label>Post Graduate Course-School Organization : Administration and Management</label><br>      <input type="checkbox" value="TEFL"><label>TEFL</label><br>      <input type="checkbox" value="TESOL Certification"><label>TESOL Certification</label><br><input type="checkbox" value="TESOL Masters"><label>TESOL Masters</label><br></div>'; 

   const bangaloreCheckBoxes =
   '<div class="form-group"><label>Products</label><br><input type="checkbox" onchange="selectAll(this);" value=""><label>Select All</label><br><input type="checkbox" value="Certification Course-Montessori Teacher Training"><label>Certification Course-Montessori Teacher Training</label><br><input type="checkbox" value="Graduate Course-Montessori Teacher Training"><label>Graduate Course-Montessori Teacher Training</label><br><input type="checkbox" value="Post Graduate Course-Montessori Teacher Training"><label>Post Graduate Course-Montessori Teacher Training</label><br><input type="checkbox" value="Certification Course-Pre and Primary Teacher Training">      <label>Certification Course-Pre and Primary Teacher Training</label><br>      <input type="checkbox" value="Graduate Course-Pre and Primary Teacher Training">      <label>Graduate Course-Pre and Primary Teacher Training</label><br>      <input type="checkbox" value="Post Graduate Course-Pre and Primary Teacher Training">      <label>Post Graduate Course-Pre and Primary Teacher Training</label>      <br>      <input type="checkbox" value="Certification Course-Early Childhood Education and Care">      <label>Certification Course-Early Childhood Education and Care</label><br>      <input type="checkbox" value="Graduate Course-Early Childhood Education and Care">      <label>Graduate Course-Early Childhood Education and Care</label><br>      <input type="checkbox" value="Post Graduate Course-Early Childhood Education and Care">      <label>Post Graduate Course-Early Childhood Education and Care</label><br>      <input type="checkbox" value="Certification Course-Nursery Teacher Training">      <label>Certification Course-Nursery Teacher Training</label><br>      <input type="checkbox" value="Graduate Course-Nursery Teacher Training">      <label>Graduate Course-Nursery Teacher Training</label><br>      <input type="checkbox" value="Post Graduate Course-Nursery Teacher Training">      <label>Post Graduate Course-Nursery Teacher Training</label><br>      <input type="checkbox" value="Certification Course-School Organization : Administration and Management">      <label>Certification Course-School Organization : Administration and Management</label><br>      <input type="checkbox" value="Graduate Course-School Organization : Administration and Management">      <label>Graduate Course-School Organization : Administration and Management</label><br>      <input type="checkbox" value="Post Graduate Course-School Organization : Administration and Management">      <label>Post Graduate Course-School Organization : Administration and Management</label><br>      <input type="checkbox" value="TEFL"><label>TEFL</label><br>      <input type="checkbox" value="TESOL Certification"><label>TESOL Certification</label><br><input type="checkbox" value="TESOL Masters"><label>TESOL Masters</label><br></div>'; 

   const atheneumUKCheckBoxes =
   '<div class="form-group"><label>Products</label><br><input type="checkbox" onchange="selectAll(this);" value=""><label>Select All</label><br><input type="checkbox" value="Certification Course-Montessori Studies"><label>Certification Course-Montessori Studies</label><br><input type="checkbox" value="Graduate Course-Montessori Studies"><label>Graduate Course-Montessori Studies</label><br><input type="checkbox" value="Post Graduate Course-Montessori Studies"><label>Post Graduate Course-Montessori Studies</label><br><input type="checkbox" value="Certification Course-Pre and Primary Education">      <label>Certification Course-Pre and Primary Education</label><br>      <input type="checkbox" value="Graduate Course-Pre and Primary Education">      <label>Graduate Course-Pre and Primary Education</label><br>      <input type="checkbox" value="Post Graduate Course-Pre and Primary Education">      <label>Post Graduate Course-Pre and Primary Education</label>      <br>      <input type="checkbox" value="Certification Course-Early Childhood Education and Care">      <label>Certification Course-Early Childhood Education and Care</label><br>      <input type="checkbox" value="Graduate Course-Early Childhood Education and Care">      <label>Graduate Course-Early Childhood Education and Care</label><br>      <input type="checkbox" value="Post Graduate Course-Early Childhood Education and Care">      <label>Post Graduate Course-Early Childhood Education and Care</label><br>      <input type="checkbox" value="Certification Course-Nursery Teacher Training">      <label>Certification Course-Nursery Teacher Training</label><br>      <input type="checkbox" value="Graduate Course-Nursery Teacher Training">      <label>Graduate Course-Nursery Teacher Training</label><br>      <input type="checkbox" value="Post Graduate Course-Nursery Teacher Training">      <label>Post Graduate Course-Nursery Teacher Training</label><br>      <input type="checkbox" value="Certification Course-School Organization : Administration and Management">      <label>Certification Course-School Organization : Administration and Management</label><br>      <input type="checkbox" value="Graduate Course-School Organization : Administration and Management">      <label>Graduate Course-School Organization : Administration and Management</label><br>      <input type="checkbox" value="Post Graduate Course-School Organization : Administration and Management">      <label>Post Graduate Course-School Organization : Administration and Management</label><br>  <input type="checkbox" value="Certification Course-Child Development Associate">      <label>Certification Course-Child Development Associate</label><br>      <input type="checkbox" value="Graduate Course-Child Development Associate">      <label>Graduate Course-Child Development Associate</label><br>      <input type="checkbox" value="Post Graduate Course-Child Development Associate">      <label>Post Graduate Course-Child Development Associate</label><br>  <input type="checkbox" value="Certification Course-Business Studies">      <label>Certification Course-Business Studies</label><br>      <input type="checkbox" value="Graduate Course-Business Studies">      <label>Graduate Course-Business Studies</label><br>      <input type="checkbox" value="Post Graduate Course-Business Studies">      <label>Post Graduate Course-Business Studies</label><br>  <input type="checkbox" value="TEFL"><label>TEFL</label><br>      <input type="checkbox" value="TESOL Certification"><label>TESOL Certification</label><br><input type="checkbox" value="TESOL Masters"><label>TESOL Masters</label><br></div>'; 
</script>


<!-- Vue js code -->




<?php endif; ?>