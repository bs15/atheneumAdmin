<?php if (!$_SESSION['LoggedIn']){
  header("Location: signIn");
 }
?>

<?php if($_SESSION['LoggedIn']): ?>
<div class="container" id="vapp">
	<div class="card shadow mb-3 pb-3">
		<div class="card-body">
      <h3 class="text-center">Manage Holidays</h3>
      <div class="alert alert-warning text-center" v-if="isMainMsg">{{ msg }}</div>
      <hr>
      <div class="text-center">
  		  <h5 class="">Weekly Holidays <button data-toggle="modal" data-target="#exampleModal" style="border-radius: 50%;" class="btn btn-primary" title="Chnage Weekly Holidays"><i class="fa fa-pencil" aria-hidden="true"></i>
          </button>
        </h5>
        <ul class="col-md-6 col-sm-12 mx-auto list-group">
          <li class="list-group-item list-group-item-secondary" v-for="day in weekHolidays">{{ day }}</li>
        </ul>
      </div>
      <hr>
      <div class="text-center">
        <h5>Other Holidays(<?php echo date("Y"); ?>)</h5>
        <button type="button" class="font-weight-bold btn btn-primary" @click="addHolidays"><i class="fa fa-plus" aria-hidden="true"></i> Add More Holidays</button>
        <div class="text-center mt-2">
          <div class="row col-md-10 col-sm-12 mx-auto mb-2" v-for="(extra,counter) in extraHolidays" v-bind:key="counter">
            <div class="col-md-5 col-sm-12">
              <input type="text" v-model="extra.occassion" required class="form-control shadow-sm" placeholder="Add another occassion"> 
            </div>
            <div class="col-md-5 col-sm-12">
              <v-date-picker
                disabled=true
                mode='multiple'
                color="red"
                is-dark
                v-model='extra.dates'
              />
             <!--  <input type="text" required  class="date form-control  shadow-sm" placeholder="Select Date"> -->
            </div>
            <div class="col-md-2 col-sm-12 mx-auto text-center">
              <button class="btn btn-danger" type="button" @click="deleteProf(counter)"><i class="fa fa-trash" aria-hidden="true"></i></button>
            </div>
          </div><br>
          <div class="text-center">
            <p style="font-weight: bold;">{{ msgToShow }}</p>
          </div>
          <div class="text-center">
            <button @click="saveHolidays" class="btn btn-success" v-if="extraHolidays.length>0">Save Holidays</button>
            <button @click="saveHolidays" class="btn btn-success" disabled=true v-else>Save Holidays</button>
          </div>
        </div>
      </div>
		</div>
	</div>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Set Weekly Holidays</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="text-center">
            <p style="font-weight: bold;">{{ msgInModal }}</p>
          </div>
       <div class="form-group text-center mx-auto">
          <label style="font-size: 18px;" class="text-center">Update your Weekly holidays</label>
           <v-select multiple v-model="selectedweekdays" placeholder="Type Week day" :options="weekdays"></v-select><br>
           <button class="btn btn-primary" @click="setWeeklyHolidays">Set Holidays</button>
            <!--  -->
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>

<script>
Vue.component('v-select', VueSelect.VueSelect);

const vueApp = new Vue({
  el: '#vapp',

  data: { 
   display: 'redbox',
   dates:[],
   successMsg:null,
   showMessage:false,
   ErrorMsg: null,
   selectedweekdays: [],
   showLoader:false,
   disableButton:false,
   extraHolidays:[
   ],
   weekdays:["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
   weekHolidays:[],
   downloadLinkUrl: 'https://bscdn.sgp1.digitaloceanspaces.com/',
   dayExists:false,
   msg:null,
   msgInModal: null,
   isMainMsg: false,
   msgToShow:null,
  },
  methods: { 
    formatDate(date) {
      var dateString = new Date(date.getTime() - (date.getTimezoneOffset() * 60000 ))
                          .toISOString()
                          .split("T")[0];

      // console.log(dateString); 
      return dateString;
    },
    makeDate(date){
      // return new Date(date);
      var dateArr = date.split("-");
      newDate = new Date(dateArr[0], dateArr[1], dateArr[2])
      return newDate;
    },
    fetchHolidays(){
      this.isMainMsg = false;
      this.msgToShow = null;
      this.msgInModal = null;
      this.extraHolidays = [];
      // this.weekHolidays = ["Saturday", "Sunday"];
      var self = this;
      fetch('/API/V1/?fetchCentralHolidays')
      .then(function(response) {
        if (response.status !== 200) {
          console.log(
            "Looks like there was a problem. Status Code: " + response.status
          );
          return;
        }
        response.json().then(function(data) {
          // console.log( JSON.parse(data.data['CONTENT_JSON']).otherdays);
          // console.log(JSON.parse(data.data['CONTENT_JSON']).weekdays.split(","));
          if (data.errorm) {
            self.isMainMsg = true;
            self.msg = data.errorm;
          }else{
            self.weekHolidays = JSON.parse(data.data['CONTENT_JSON']).weekdays.split(",");
            var otherDays = JSON.parse(data.data['CONTENT_JSON']).otherdays;
            otherDays.forEach((oneday, index) =>{
              self.extraHolidays.push({
                occassion:oneday.occassionName,
                dates:
                  oneday.occassionDate.map(self.makeDate)
              });
            })
          }
        });
      })
      .catch(function(err) {
        console.log("Fetch Error :-S", err);
      });
    },

    // Save weekly holidays

    setWeeklyHolidays(){
      var jsonObj = {};
      var self = this;
      self.msgInModal = null;
      self.msgToShow = null;
      // console.log(self.selectedweekdays);
      if (typeof self.selectedweekdays !== 'undefined' && self.selectedweekdays.length > 0) {
          let formData = new FormData();
          formData.append('formName', 'setCentralWeeklyHolidays');
          formData.append('weekDays', self.selectedweekdays);
          fetch("/API/V1/", {
              method: "POST",
              body:formData,
          }).then(
              function(response) {
              response.json().then(function(data) {
                // console.log(data);
                if (data.errorm) {
                  self.msgInModal = data.errorm;
                }else{
                  self.weekHolidays = [];
                  self.fetchHolidays();
                  console.log(data.data)
                  self.msgInModal = "Successfully saved holidays";
                }
              });
            }
          )
          .catch(function(err) {
            console.log('Fetch Error :-S', err);
          });
      }else{
        self.msgInModal = "Please select atleast one day";
      }
    },


    addHolidays(){
      this.extraHolidays.push({
        occassion:'',
        dates:[]
      });

      // console.log(this.extraHolidays);
    },
    saveHolidays(){
      this.msgToShow = null;
      // console.log(this.extraHolidays);
      var selectedOtherHolidays = [];
      var self = this;
      // filtering throufgh empty entries and formatting dates
      this.extraHolidays.forEach((oneday, index) => {
        // console.log(oneday.dates.map(this.formatDate));
        if (oneday.dates.length == 0 || oneday.occassion == "") {
          self.msgToShow = "Please fill all the fields";
        }else{
          var newDate = oneday.dates.map(this.formatDate);
          // console.log(oneday.occassion)
          var newOccassion = oneday.occassion;
          var newOb = {occassionName:newOccassion, occassionDate: newDate};
          selectedOtherHolidays.push(newOb);
        }
      })
      if (selectedOtherHolidays.length > 0) {
        console.log(selectedOtherHolidays)
        selectedOtherHolidays = JSON.stringify(selectedOtherHolidays)
        let formData = new FormData();
        formData.append('formName', 'setCentralOtherHolidays');
        formData.append('otherHolidays', selectedOtherHolidays);
        fetch("/API/V1/", {
            method: "POST",
            body:formData,
        }).then(
            function(response) {
            response.json().then(function(data) {
              console.log(data);
              if (data.errorm) {
                self.msgToShow = data.errorm;
              }else{
                // self.weekHolidays = [];
                // console.log(data.data)
                self.msgToShow = "Successfully saved holidays";
                self.fetchHolidays();
              }
            });
          }
        )
        .catch(function(err) {
          console.log('Fetch Error :-S', err);
        });
      }
    },
    deleteProf(counter){
      this.extraHolidays.splice(counter,1);
    },
  },
  created(){
    // this.fetchVideos();
    this.fetchHolidays();
  },
})
</script>
<style scoped>
  .drag{
    /*position: absolute;
    top: 50%;
    left: 50%;
    margin-top: -100px;
    margin-left: -250px;*/
    /*width: 500px;*/
    height: 200px;
    border: 4px dashed black;
  }
  .drag p{
    width: 100%;
    height: 100%;
    text-align: center;
    line-height: 170px;
    color: black;
    font-family: Arial;
  }
  .drag input{
    position: absolute;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
    opacity: 0;
  }
</style>

<?php endif; ?>