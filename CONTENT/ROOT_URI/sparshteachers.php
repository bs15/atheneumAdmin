<script>
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
</script>
<?php if (!$_SESSION['LoggedIn']){
 	header("Location: signIn");
 }
?>

<?php if($_SESSION['LoggedIn']): ?>

<div class="container-fluid" id="vue">
  <div id="fb-root"></div>
	
	 <div class="row">
        <div class="col-md-12">
          <h1 class="display-7 text-center">Sparsh Teachers</h1>

          <!-- ---------------USER DETAILS TABLE ------------- -->

          <div class="row">
          <div class="col-12 ml-auto mr-auto">
          	<div class="ml-auto text-center">
          		<div class="btn-group">
          			<button class="btn btn-info" data-toggle="modal" data-target="#modal2">Add Teacher</button>
          		</div>
            </div><br>
            <div class="ml-auto">
              <button class="btn btn-outline-danger " onclick="exportTableToCSV('sparshTeachers.csv')">Export Data To CSV File</button>
            </div><br>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Sparsh Teachers</h3>
                <div class="card-tools">
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               <div class=" table-responsive p-0">
                <table class="display table table-hover text-nowrap" id="sparshUsers">
                  <thead>
			            <tr>
			                <th>ID</th>
			                <th>Name</th>
			                <th>Email</th>
			                <th>Phone</th>
			                <th>Class</th>
			                <th>Center</th>
			                <th>Reg. date</th>
			                <th>Status</th>
                      <th>Action</th>
			                
			            </tr>
			        </thead>
			        <tbody id="userListBody">
			        	
			        </tbody>
			        <tfoot>
			            <tr>
			                <th>ID</th>
			                <th>Name</th>
			                <th>Email</th>
			                <th>Phone</th>
			                <th>Class</th>
			                <th>Center</th>
			                <th>Reg. date</th>
			                <th>Status</th>
                      <th>Action</th>
			            </tr>
			        </tfoot>
                </table>
              </div>

              </div> 
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
          

        </div>
      </div>
</div>

<!-- Add parent modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Teacher</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php 
      $partnerCodes = array();
      $sqlPartner = "SELECT * FROM SPARSH_PARTNERS";
      $resultPartner = mysqli_query($link, $sqlPartner);

       ?>
      <div class="modal-body">
        <form class="form" method="POST">
          <div class="col-md-8 col-sm-12 mx-auto">
            <div class="alert alert-warning mx-auto" id="formWarning" style="display: none;"></div>
            <div class="alert alert-success mx-auto" id="formSuccess" style="display: none;"></div>
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Teacher Name" id="teacherName" required>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <input type="email" class="form-control" placeholder="Email" id="email" required>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <input type="number" class="form-control" placeholder="Phone Number" id="phone" required>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <select class="form-control" id="selcetedClass" onchange="selectClass();">
                    <option value="null">Select Class</option>
                    <option value="PG">PG</option>
                    <option value="IK1">IK1</option>
                    <option value="IK2">IK2</option>
                    <option value="IK3">IK3</option>
                  </select>

                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <select class="form-control" id="partnerCode" onchange="selectPartner();">
                    <option>Select partner code</option>
                    <?php 
                    while ($rowPartner = mysqli_fetch_array($resultPartner,MYSQLI_ASSOC)) { 
                      echo '<option value="'.$rowPartner["PARTNER_CODE"].'">'.$rowPartner["PARTNER_CODE"].'</option>';
                    }

                     ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 mx-auto">
                <div class="form-group">
                  <input type="date" class="form-control" placeholder="Reg Date" value="<?php echo date("Y-m-d"); ?>" id="regDate" required>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <p id="saveMsg" style="display: none; font-weight: bold;">Saving Data...</p><button type="button" id="saveBtn" onclick="addTeacher();" class="btn btn-primary">Save changes</button> 
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Teacher Details</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           <div class="alert alert-warning mx-auto" id="formWarning2" style="display: none;"></div>
            <div class="alert alert-success mx-auto" id="formSuccess2" style="display: none;"></div>
          <form>
            <input type="hidden" id="teacherId">
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <label>Teacher Name</label>
                  <input type="text" class="form-control" placeholder="Teacher Name" id="teacherNameEdit" required>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <label>Teacher Email</label>
                  <input type="email" class="form-control" placeholder="Email" id="emailEdit" required>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <label>Teacher Phone</label>
                  <input type="number" class="form-control" placeholder="Phone Number" id="phoneEdit" required>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <label>Class</label>
                  <input type="text" class="form-control" id="selectedClassEdit" placeholder="Class">

                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <label>Partner Code</label>
                  <input type="text" class="form-control" id="partnerCodeEdit" class="Partner Code">
                </div>
              </div>
              <div class="col-md-6 col-sm-12 mx-auto">
                <div class="form-group">
                  <label>Session Date</label>
                  <input type="date" class="form-control" placeholder="Session Date"  id="sessionDateEdit" required >
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <p id="saveMsg2" style="display: none; font-weight: bold;">Saving Data...</p>
          <button id="saveBtn2" type="button" onclick="saveTeacherData();" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
  alert = function() {};
  var teacherName = null;
  var email = null;
  var selcetedClass = null;
  var phone = null;
  var partnerCode = null;
  var password = null;
  var regDate = null;
  var formWarning = document.getElementById('formWarning');
  var formSuccess = document.getElementById('formSuccess');
  var saveMsg = document.getElementById('saveMsg');
  var saveBtn = document.getElementById('saveBtn');
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  function selectClass(){
    var e = document.getElementById("selcetedClass");
    selcetedClass = e.options[e.selectedIndex].value;
    console.log(selcetedClass);
  }
  function selectPartner(){
    var e = document.getElementById("partnerCode");
    partnerCode = e.options[e.selectedIndex].value;
    console.log(partnerCode);
  }


function fetchData(argument) {
	$.ajax({ 
      url: "/API/V1/?sparshAllTeachers",
      dataType:"html",
      type: "post",
      success: function(data){
        var table = $('#sparshUsers');
        var body = $('#userListBody');
        table.find("tbody tr").remove();
        table.find("tbody div").remove();
        body.append(data);
        $('#sparshUsers').DataTable( {
	        "order": [[ 0, "desc" ]],
	        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    	});
      }
    });
}
	
$(document).ready(fetchData());

function editData(id){
	
}
function generatePassword(length){
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function addTeacher(){
    saveMsg.style.display = "inline-block";
    saveMsg.innerHTML = 'Saving Data...';
    saveBtn.disabled = true;
    formWarning.style.display = 'none';
    formSuccess.style.display = 'none';
    teacherName = document.getElementById('teacherName').value; 
    email = document.getElementById('email').value; 
    phone = document.getElementById('phone').value; 
    regDate = document.getElementById('regDate').value; 
    password = generatePassword(6); 
    if (!teacherName || !email || !phone || !selcetedClass || !regDate || !partnerCode) {
      formWarning.style.display = 'inline-block';
      formWarning.innerHTML = 'Please fill all the fields';
      saveMsg.style.display = "none";
      saveBtn.disabled = false;
    }
    else if(reg.test(email) == false){
      formWarning.style.display = 'inline-block';
      formWarning.innerHTML = 'provide a valid email';
      saveMsg.style.display = "none";
      saveBtn.disabled = false;
    }

    else{
      formWarning.style.display = 'none';
      let formData = new FormData();
      formData.append("formName", "registerTeacher");
        formData.append("teacherName", teacherName);
        formData.append("selcetedClass", selcetedClass);
        formData.append("email", email);
        formData.append("phone", phone);
        formData.append("partnerCode", partnerCode);
        formData.append("password", password);
        formData.append("regDate", regDate);
        fetch('/API/V1/', {
          method: "POST",
          body: formData
        }).then(function(response) {
          if (response.status !== 200) {
            console.log(
              "Looks like there was a problem. Status Code: " + response.status
            );
            return;
          }
          response.json().then(function(data) {
          console.log(data);
          saveBtn.disabled = false;
          saveMsg.style.display = "none";
          if (data.errorm != null) {
            formWarning.style.display = 'inline-block';
            formWarning.innerHTML = data.errorm;
          } else {
            formWarning.style.display = 'none';
            formSuccess.style.display = 'inline-block';
            formSuccess.innerHTML = data.data;
            sendMailToTeacher();
            document.getElementById('teacherName').value = null;
            document.getElementById('partnerCode').value = null;
            document.getElementById('email').value = null;
            document.getElementById('phone').value = null;
            document.getElementById('selcetedClass').value = null;
            document.getElementById('regDate').value = null;
            fetchData();
                  // self.setCookie();
            // self.$router.push("verifyMail");
          }
        });
    });
  }
}
function sendMailToTeacher(){
  let formData = new FormData();
  formData.append("sendMail", "true");
  formData.append("reciever", email);
  formData.append("sender", "Sparsh");
  formData.append("senderMail", "no-reply@teenybeans.in");
  formData.append("subject", "Sparsh Registration Details");
  formData.append(
    "message",
    "<html><body><h2>Welcome to Sparsh - the parent App. Sparsh is an APP connecting parents with Teeny Beans' internationally recognized preschool curriculum. You have been registered as a teacher and provided the login credentials for Sparsh.</h2><h2>Sparsh Download Link:- <a href='https://play.google.com/store/apps/details?id=com.beanstalkedu.app.sparsh'>Click here to Download</a></h2><h2>Login Id:- "+email+"</h2><h2>Password:- "+password+"</h2><p>For more information about us please visit <a href='https://teenybeans.in/'>our website</a>.<br>Thank you.<br>-Team Teenybeans.</p></body></html>"
  );
  fetch("https://mailapi.teenybeans.in/", {
    method: "POST",
    body: formData
  })
  .then(function(response) {
    response.json().then(function(data) {
    });
  })
  .catch(function(err) {
    console.log("Fetch Error :-S", err);
  });
}

  var teacherNameEdit = null;
  var emailEdit = null;
  var selcetedClassEdit = null;
  var phoneEdit = null;
  var partnerCodeEdit = null;
  var sessionDateEdit = null;
  var formWarning2 = document.getElementById('formWarning2');
  var formSuccess2 = document.getElementById('formSuccess2');
  var saveMsg2 = document.getElementById('saveMsg2');
  var saveBtn2 = document.getElementById('saveBtn2');

  function fetchSingleTeacher(id){
    fetch('/API/V1/?fetchSingleParent&id='+id)
      .then(
        function(response) {
          if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' +
              response.status);
            return;
          }
          // Examine the text in the response
          response.json().then(function(data) {
            if(data){
              console.log(data.data)
              document.getElementById('teacherNameEdit').value = data.data.PARENT_NAME;
              document.getElementById('partnerCodeEdit').value = data.data.CENTER_CODE;
              document.getElementById('emailEdit').value = data.data.EMAIL;
              document.getElementById('phoneEdit').value = data.data.PHONE;
              document.getElementById('selectedClassEdit').value = data.data.COURSE_NAME;
              document.getElementById('sessionDateEdit').value = data.data.SESSION_DATE;
              document.getElementById('teacherId').value = data.data.USER_ID;
            }else{
              // self.noticeAlert = "No blog present. Please create one!";
              // self.showDismissibleAlert = true;
              // self.pages = null;
            }
            
          });
        }
      )
      .catch(function(err) {
        console.log('Fetch Error :-S', err);
      });
   }

   function saveTeacherData(){
     saveMsg2.style.display = "inline-block";
      saveMsg2.innerHTML = 'Saving Data...';
      saveBtn2.disabled = true;
      formWarning2.style.display = 'none';
      formSuccess2.style.display = 'none';
      teacherId = document.getElementById('teacherId').value; 
      teacherNameEdit = document.getElementById('teacherNameEdit').value; 
      emailEdit = document.getElementById('emailEdit').value; 
      phoneEdit = document.getElementById('phoneEdit').value; 
      partnerCodeEdit = document.getElementById('partnerCodeEdit').value; 
      selectedClassEdit = document.getElementById('selectedClassEdit').value; 
      sessionDateEdit = document.getElementById('sessionDateEdit').value; 
      // console.log(partnerCodeEdit)
      if (!teacherNameEdit || !partnerCodeEdit || !emailEdit || !phoneEdit || !selectedClassEdit) {
        formWarning2.style.display = 'inline-block';
        formWarning2.innerHTML = 'Please fill all the fields';
        saveMsg2.style.display = "none";
        saveBtn2.disabled = false;
      }else{
        formWarning2.style.display = 'none';
        let formData = new FormData();
        formData.append("formName", "updateTeacher");
        formData.append("teacherId", teacherId);
        formData.append("teacherName", teacherNameEdit);
        formData.append("email", emailEdit);
        formData.append("phone", phoneEdit);
        formData.append("partnerCode", partnerCodeEdit);
        formData.append("class", selectedClassEdit);
        formData.append("sessionDate", sessionDateEdit);
        fetch('/API/V1/', {
          method: "POST",
          body: formData
        }).then(function(response) {
          if (response.status !== 200) {
            console.log(
              "Looks like there was a problem. Status Code: " + response.status
            );
            return;
          }
          response.json().then(function(data) {
            console.log(data);
            saveBtn2.disabled = false;
            saveMsg2.style.display = "none";
            if (data.errorm != null) {
              formWarning2.style.display = 'inline-block';
              formWarning2.innerHTML = data.errorm;
            } else {
              formWarning2.style.display = 'none';
              formSuccess2.style.display = 'inline-block';
              formSuccess2.innerHTML = data.data;
              // sendMailToPartner();
              fetchData();
              fetchSingleTeacher(teacherId);
            }
         });
      });
      }
   }




function deleteTeacher(teacherId) {
    // console.log(parentId)
      var result = confirm('Are you sure you want to delete the parent record?');
      if (result) {
        let formData = new FormData();
          formData.append("formName", "deleteUser");
          formData.append("userId", teacherId);
        fetch('/API/V1/', {
            method: "POST",
            body: formData
          }).then(function(response) {
            if (response.status !== 200) {
              console.log(
                "Looks like there was a problem. Status Code: " + response.status
              );
              return;
            }
            response.json().then(function(data) {
              console.log(data);
              fetchData();
           });
        });
      }
   }
</script>

<?php endif; ?>