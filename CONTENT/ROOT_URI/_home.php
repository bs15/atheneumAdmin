<script>
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
</script>
<script type="text/javascript">
  
</script>
<?php 
	$link = mysqli_connect(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
  $email = $_SESSION["user"];
  $userId = $_SESSION["userId"];
  $sql = "SELECT * FROM ATHENEUM_ADMIN WHERE UNI_ID = '$userId'";
  $result = mysqli_query($link,$sql);

 if($result){
    if(mysqli_num_rows($result)>0){
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $userName = $row["NAME"];
      $email = $row["EMAIL"];
      $phone = $row["PHONE"];
    }
  }
 ?>

 <?php if (!$_SESSION['LoggedIn']){
 	header("Location: signIn");
 }


 ?>

<?php if($_SESSION['LoggedIn']): ?>

<div class="container">
  <div id="fb-root"></div>
	
	 <div class="row">
        <div class="col-md-12">
          <h1 class="display-7">Admin Dashboard</h1>
          <div class="user">
            <p class="lead text-muted">Welcome <?php echo $userName; ?></p>
            <a href="changePassword" class="btn btn-danger"><i class="fas fa-lock"></i>&nbsp;Change Password</a>
                <!-- <a href="changePassword" class="btn btn-warning"><i class="fas fa-user"></i>&nbsp;Update Details</a> -->
          </div>
          <br>

          <!-- ---------------USER DETAILS TABLE ------------- -->

          <div class="row">
          <div class="col-12 ml-auto mr-auto">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Admin Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
                <div class="row">
                  <div class="col">
                    <h5><b>User Name:- </b><?php echo $userName; ?></h5>
                  </div>
                  <div class="col">
                    <h5><b>Email:- </b><?php echo $email; ?></h5>
                  </div>
                  <div class="col">
                    <h5><b>Phone:- </b><?php echo $phone; ?></h5>
                  </div>
                </div>
              </div> 
              <!-- /.card-body -->
            </div>
            <div class="ml-auto">
              <button class="btn btn-outline-danger " onclick="exportTableToCSV('AtheneumUserDetails.csv')">Export Data To CSV File</button>
            </div><br>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Atheneum Students</h3>
                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 180px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="User Name" id="searchUser">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
               <div class="table-responsive p-0">
                <table class="display table table-hover text-nowrap" id="atheneumStudents">
                  <thead>
                  <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Name</th>
                      <th scope="col">Email ID's</th>
                      <th scope="col">Phone</th>
                      <th scope="col">Date</th>
                      <th scope="col">Status</th>
                      <th scope="col">Partner</th>
                      <th scope="col">Action</th>
                      
                  </tr>
              </thead>
              <tbody id="userListBody">
                
              </tbody>
              <tfoot>
                  <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Name</th>
                      <th scope="col">Email ID's</th>
                      <th scope="col">Phone</th>
                      <th scope="col">Date</th>
                      <th scope="col">Status</th>
                      <th scope="col">Partner</th>
                      <th scope="col">Action</th>
                  </tr>
              </tfoot>
                </table>
              </div>

              </div> 
              
            </div>
            <!-- /.card -->
          </div>
        </div>
          

        </div>
      </div>
</div>
<?php else: ?>
  <div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12 ml-auto mr-auto">
      <div class="register-logo">
        <h2><b>Atheneum Global Teacher Training College</b></h2>
        <p>Admin Dashboard portal</p>
      </div>
      <div class="card">
        <div class="card-body">
          <div class="ml-auto mr-auto text-center">
            <img src="/IMAGES/logo.jpeg" width="50%" height="50%">
          </div>
          <div class="card-text">Welcome Admin . Please <a href="signIn">Login</a> to continue</div>
         
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
  </div>

<?php endif; ?>
<script type="text/javascript">
  alert = function() {};
  function fetchData() {
    $.ajax({ 
        url: "/API/V1/?userList",
        dataType:"html",
        type: "post",
        success: function(data){
          var table = $('#atheneumStudents');
          var body = $('#userListBody');
          table.find("tbody tr").remove();
          table.find("tbody div").remove();
          body.append(data);
          $('#atheneumStudents').DataTable( {
            "order": [[ 0, "desc" ]],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        });
        }
      });
  }
  
  $(document).ready(fetchData());
   $(function(){
      $("#upload_pro").on('click', function(e){
          e.preventDefault();
          $("#upload1:hidden").trigger('click');
      });
    });
   function deleteUser(userId){
    console.log(userId);
   }
</script>
