<script>
  if ( window.history.replaceState ) {
      window.history.replaceState( null, null, window.location.href );
  }
</script>


<?php 
if (!$_SESSION['LoggedIn']) {
   echo '<script>window.location.href="signIn"</script>';
} 
?>
<?php if ($_SESSION['LoggedIn']): ?>
  <?php 
    $currYear = date("Y");
    $nextYear = date('Y', strtotime('+1 year'));
    $currMonth = date('m');
    $months = array('1' => 'January', '2' => 'February', '3' => 'March', '4' => 'April', '5' => 'May', '6' => 'June', '7' => 'July', '8' => 'August', '9' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
  ?>
  
<div class="container" id="vapp">
  <div class="card mt-2 shadow mb-2 bg-white rounded">
    <div class="card-body">
      <h2 class="text-center">Select class</h2>
        <div class="row col-md-8 mx-auto">
          <div class="col-md-3 col-sm-6 mx-auto mb-2">
            <button class="btn btn-success btn-lg btn-block shadow" @click="selectPG">PG</button>
          </div>
          <div class="col-md-3 col-sm-6 mx-auto mb-2">
            <button class="btn btn-primary btn-lg btn-block shadow" @click="selectIK1">IK1</button>
          </div>
          <div class="col-md-3 col-sm-6 mx-auto mb-2">
            <button class="btn btn-dark btn-lg btn-block shadow" @click="selectIK2">IK2</button>
          </div>
          <div class="col-md-3 col-sm-6 mx-auto mb-2">
            <button class="btn btn-info btn-lg btn-block shadow" @click="selectIK3">IK3</button>
          </div>
        </div>
    </div>
  </div>
  <div class="card shadow" v-if="showDays">
    <div class="card-body">
      <h3 class="text-center">Selected Class:- {{selectedClass}}</h3><hr>
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default border-bottom" v-for="index in 42" :key="index">
          <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
            <a class="collapsed menuHeading" style="text-decoration: none;" role="button" data-toggle="collapse" data-parent="#accordion" :href="'#area'+index" aria-expanded="false" :aria-controls=index>
              Week #{{index}}
            </a>
          </h4>
          </div>
          <div :id="'area'+index" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body px-5 mb-2">
              <div class="row mx-auto">
                <div class="col-md-2 col-sm-4">
                  <button class="btn btn-outline-primary btn-block btn-lg" data-toggle="modal" data-target="#exampleModal" @click="daySelected('week'+index+'day1')">Day 1</button>
                </div>
                <div class="col-md-2 col-sm-4">
                  <button class="btn btn-outline-warning btn-block btn-lg" data-toggle="modal" data-target="#exampleModal" @click="daySelected('week'+index+'day2')">Day 2</button>
                </div>
                <div class="col-md-2 col-sm-4">
                  <button class="btn btn-outline-info btn-block btn-lg" data-toggle="modal" data-target="#exampleModal" @click="daySelected('week'+index+'day3')">Day 3</button>
                </div>
                <div class="col-md-2 col-sm-4">
                  <button class="btn btn-outline-dark btn-block btn-lg" data-toggle="modal" data-target="#exampleModal" @click="daySelected('week'+index+'day4')">Day 4</button>
                </div>
                <div class="col-md-2 col-sm-4">
                  <button class="btn btn-outline-danger btn-block btn-lg" data-toggle="modal" data-target="#exampleModal" @click="daySelected('week'+index+'day5')">Day 5</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg " role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">{{selectedClass+' - '+selectedDay}}</h5>
          <br>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div v-if="showModalLoader">
          <img src="/IMAGES/loader.gif" class="text-center" width="" id="loader">
        </div>
        <div class="modal-body" v-if="showModalContent">
          <h5>Manage content for the day</h5><hr>
          <div class="alert alert-primary" v-if="showMessage" v-html="responseFromServer">
            
          </div>
          <form method="POST" enctype="multipart/form-data">
            <div class="form-group" v-if="fetchedContentBook == null">
              <label>#1 Upload Content Book Images</label>
              <input type="file" ref="contentbookImg" class="form-control-file btn btn-success" name="" multiple>
            </div>
            <div v-else>
              <h3>Content Book Images</h3>
              <div v-for="image in fetchedContentBook">
                <p>#{{image}}</p>
                <div class="btn-group">
                   <a class="btn btn-success" :href="downloadLinkUrl+image">Download File</a>
                   <button type="button" class="btn btn-primary" @click="deleteFile(image)">Delete File</button>
                </div>
                <hr>
              </div>
              <label># Upload More Content Book Images</label>
              <input type="file" ref="contentbookImg" class="form-control-file btn btn-success" name="" multiple>
            </div><hr>
            <div class="form-group" v-if="fetchedWorkBook == null">
              <label>#2 Upload Work Book Images</label>
              <input type="file" ref="workbookImg" class="form-control-file btn btn-info" name="" multiple>
            </div>
            <div v-else>
              <h3>Work Book Images</h3>
              <div v-for="image in fetchedWorkBook">
                <p>#{{image}}</p>
                <div class="btn-group">
                   <a class="btn btn-success" :href="downloadLinkUrl+image">Download File</a>
                   <button type="button" class="btn btn-primary" @click="deleteFile(image)">Delete File</button>
                </div>
                <hr>
              </div>
              <label># Upload More Work Book Images</label>
              <input type="file" ref="workbookImg" class="form-control-file btn btn-success" name="" multiple>
            </div><hr>
            <div class="form-group" v-if="fetchedLessonPlan == null">
              <label>#3 Upload Lesson Plan Images</label>
              <input type="file" ref="lessonplanImg" class="form-control-file btn btn-primary" name="" multiple>
            </div>
            <div v-else>
              <h3>Lesson Plan Images</h3>
              <div v-for="image in fetchedLessonPlan">
                <p>#{{image}}</p>
                <div class="btn-group">
                   <a class="btn btn-success" :href="downloadLinkUrl+image">Download File</a>
                   <button type="button" class="btn btn-primary" @click="deleteFile(image)">Delete File</button>
                </div>
                <hr>
              </div>
              <label># Upload More Lesson Plan Images</label>
              <input type="file" ref="lessonplanImg" class="form-control-file btn btn-primary"  multiple>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <p v-if="showLoader">Loading...</p>
          <button :disabled="disableButton" type="button" @click="saveData" class="btn btn-primary">Save Data</button>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
const vueApp = new Vue({
  el: '#vapp',
  data: { 
   display: 'redbox',
   showDays: false,
   selectedClass: null,
   show: true,
   selectedDay:null,
   workbookImg: [],
   contentbookImg: [],
   lessonplanImg: [],
   responseFromServer:null,
   showMessage:false,
   showLoader:false,
   disableButton:false,
   fetchedContentBook:null,
   fetchedWorkBook:null,
   fetchedLessonPlan:null,
   showModalLoader:false,
   showModalContent:false,
   downloadLinkUrl: 'https://bscdn.sgp1.digitaloceanspaces.com/',
  },
  methods: {
    selectPG(){
      console.log("PG");
      this.showDays = true;
      this.selectedClass = "PG";
    },
    selectIK1(){
      console.log("IK1");
      this.showDays = true;
      this.selectedClass = "IK1";
    },
    selectIK2(){
      console.log("IK2");
      this.showDays = true;
      this.selectedClass = "IK2";
    },
    selectIK3(){
      console.log("IK3");
      this.showDays = true;
      this.selectedClass = "IK3";
    },
    daySelected(day){
      console.log(day);
      this.showMessage = false;
      this.responseFromServer = null;
      this.selectedDay = day;
      this.fetchDayContent();
    },
    fetchDayContent(){
      this.showModalContent = false;
      this.showModalLoader = true;
      var self = this;
       fetch('/API/V1/?getContentForTheDay&class='+this.selectedClass+'&day='+this.selectedDay)
        .then(function(response) {
          if (response.status !== 200) {
            console.log(
              "Looks like there was a problem. Status Code: " + response.status
            );
            return;
          }
          response.json().then(function(data) {
            console.log(data);
            self.fetchedContentBook = data.contentBook;
            self.fetchedWorkBook = data.workBook;
            self.fetchedLessonPlan = data.lessonPlan;
            self.showModalContent = true;
            self.showModalLoader = false;
            // console.log(self.fetchedContentBook);
          });
        })
        .catch(function(err) {
          console.log("Fetch Error :-S", err);
        });
    },
    saveData(){
      var self = this;
      this.showLoader = true;
      this.disableButton = true;
      let formData = new FormData();
      // console.log(this.$refs);

      //----------CONTENT BOOK----------------
      if (this.$refs.contentbookImg != null) {
        if (this.$refs.contentbookImg.files.length == 0) {
          this.contentbookImg = null;
        }else{
          for( var i = 0; i < this.$refs.contentbookImg.files.length; i++ ){
            let file = this.$refs.contentbookImg.files[i];
            // console.log(file);
            formData.append('contentbookImg[' + i + ']', file);
          } 
        }
      }

      //----------WORK BOOK----------------

      if (this.$refs.workbookImg != null) {
        if (this.$refs.workbookImg.files.length == 0) {
          this.workbookImg = null;
        }else{
          for( var i = 0; i < this.$refs.workbookImg.files.length; i++ ){
            let file = this.$refs.workbookImg.files[i];
            // console.log(file);
            formData.append('workbookImg[' + i + ']', file);
          } 
        }
      }

      //----------LESSON PLAN----------------

      if (this.$refs.lessonplanImg != null) {
        if (this.$refs.lessonplanImg.files.length == 0) {
          this.lessonplanImg = null;
        }else{
          for( var i = 0; i < this.$refs.lessonplanImg.files.length; i++ ){
            let file = this.$refs.lessonplanImg.files[i];
            // console.log(file);
            formData.append('lessonplanImg[' + i + ']', file);
          } 
        }
      }


      formData.append('class', this.selectedClass);
      formData.append('day', this.selectedDay);

      fetch("/API/V1/?uploadImages", {
          method: "POST",
          body:formData,
      }).then(
          function(response) {
          response.json().then(function(data) {
            console.log(data);
            self.showMessage = true;
            self.showLoader = false;
            self.disableButton = false;
            self.responseFromServer = data.result;
            self.fetchDayContent();
          });
        }
      )
      .catch(function(err) {
        console.log('Fetch Error :-S', err);
      });

      // console.log(self.contentbookImg); 
    },
    deleteFile(fileName){
      var self = this;
      let formData = new FormData();
      formData.append('fileName', fileName);

      fetch("/API/V1/?deleteFile", {
          method: "POST",
          body:formData,
      }).then(
          function(response) {
          response.json().then(function(data) {
            console.log(data);
            self.showMessage = true;
            self.showLoader = false;
            self.responseFromServer = data.result;
            self.fetchDayContent();
          });
        }
      )
      .catch(function(err) {
        console.log('Fetch Error :-S', err);
      });
    }
  },
})
</script>

<style scoped>
  .menuHeading{
    text-decoration: none;
    color: black;
  }
  #loader {
    position: absolute;
    left: 50%;
    top: 80%;
    z-index: 1;
    width: 80px;
    height: 80px;
    margin: -75px 0 0 -75px;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 80px;
    height: 80px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
  }
  .panel-default>.panel-heading {
    padding: 0;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }

.panel-default>.panel-heading a {
  display: block;
  padding: 10px 15px;
}

.panel-default>.panel-heading a:after {
  content: "";
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  float: right;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.panel-default>.panel-heading a[aria-expanded="true"] {
}

.panel-default>.panel-heading a[aria-expanded="true"]:after {
  content: "\2212";
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}

.panel-default>.panel-heading a[aria-expanded="false"]:after {
  content: "\002b";
  -webkit-transform: rotate(90deg);
  transform: rotate(90deg);
}
</style>

<?php endif ?>
