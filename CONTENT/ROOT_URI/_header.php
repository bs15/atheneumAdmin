<?php
$link = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
$link->set_charset("utf8");
$email = $_SESSION["user"];


if(mysqli_connect_error()){
   die("ERROR: UNABLE TO CONNECT: ".mysqli_connect_error());
}

 $sql = "SELECT * FROM ATHENEUM_ADMIN WHERE EMAIL = '$email'";

 $result = mysqli_query($link,$sql);

 if($result){
      if(mysqli_num_rows($result)>0){
        $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
        $userName = $row["NAME"];
        $name = explode(" ", $userName);
        $userFirst = $name[0];
      }
  }
 ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="/CSS/DIZ.css" >
    <link rel="stylesheet" href="/CSS/style.css" >
    <link rel="stylesheet" href="/CSS/table.min.css" >
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

    <link rel="stylesheet" href="/CSS/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->

    <link rel="stylesheet" href="/CSS/dist/css/adminlte.css">

    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="/CSS/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- <link rel="stylesheet" href="/CSS/video.css" > -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <!-- <script src="https://kit.fontawesome.com/e1f2cafe0a.js"></script> -->

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.js"></script>
    <script type="text/javascript" src="/JS/vue.min.js"></script>
    <script src="https://unpkg.com/vue-select@3.0.0"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-select@3.0.0/dist/vue-select.css">
    <script src='https://unpkg.com/v-calendar'></script>


   <!--  <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="  crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
       -->

    <title>Atheneum Global College</title>
  </head>
  <body class="hold-transition  layout-top-nav">
    <div class="wrapper">

  <!-- NAVBAR STARTS -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white fixed-top">
    <div class="container">
      <a href="/" class="navbar-brand">
        <img src="/IMAGES/favicon.ico" alt="" class="brand-image"
             style="height: 40px; width:auto;">
        <!-- <span class="btn btn-outline-danger brand-text">Atheneum</span> -->
      </a>
      
     

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
      <?php if ($_SESSION['LoggedIn']): ?>
        <!------------- NOTIFICATION LOGIC ----------->
        <?php 
          $sqlNoti = "SELECT * FROM ATHENEUM_NOTIFICATION WHERE USER_TYPE='ADMIN'";
          $resultNoti = mysqli_query($link, $sqlNoti);
          $active = 0;
          while($row = mysqli_fetch_array($resultNoti,MYSQLI_ASSOC)){
            if($row['STATUS'] == 'PENDING'){
              $active++;
            }
          }
         ?>
        <ul class="navbar-nav">
          <li class="nav-item dropdown" style="margin-right: 5px;">
            <a class="btn btn-outline-success dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Atheneum
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/">Students</a>
              <a class="dropdown-item" href="atheneumpartners">Partners</a>
              <a class="dropdown-item" href="teachermanage">Teachers</a>
            </div>
          </li>
          <li class="nav-item dropdown" style="margin-right: 5px;">
            <a class="btn btn-outline-success dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Sparsh
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="sparshmanage">Parents</a>
              <a class="dropdown-item" href="sparshteachers">Teachers</a>
              <a class="dropdown-item" href="sparshPartners">Partners</a>
              <a class="dropdown-item" href="uploadcontent">Content Manage</a>
              <a class="dropdown-item" href="sparshHoliday">Holiday Manage</a>
            </div>
          </li>
          <li class="nav-item">
            <a href="managecoupon" class="btn btn-outline-success">Coupon Manage</a>
          </li>
        </ul>
      <?php endif; ?>

      </div>
     

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
      <?php if ($_SESSION['LoggedIn']): ?>
       <li class="nav-item dropdown">
          <a class="btn btn-warning" data-toggle="dropdown" href="#">
            <?php echo $userFirst; ?> &nbsp;<i class="fa fa-caret-down"></i>
            <span class="badge badge-warning navbar-badge"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="signOut" class="nav-link">Sign Out</a>
            <div class="dropdown-divider"></div>
          </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" href="notification">
              <i class="far fa-bell"></i>
              <span class="badge badge-warning navbar-badge"><?php if($active !=0) echo $active; ?></span>
          </a>
        </li>
        

        
        <?php else: ?>
          <li class="nav-item">
            <a href="signIn" class="btn btn-outline-success">Sign In</a>
          </li>
        <?php endif; ?>

      </ul>
       
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
  </nav>
  <!-- /.navbar -->

   
  
  <div class="content-wrapper">
  <section class="content pt-5">
    <br>
  

