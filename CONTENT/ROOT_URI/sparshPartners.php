<script>
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
</script>
<?php if (!$_SESSION['LoggedIn']){
 	header("Location: signIn");
 }
?>

<?php if($_SESSION['LoggedIn']): ?>

<div class="container-fluid" id="vue">
  <div id="fb-root"></div>
	
	 <div class="row">
        <div class="col-md-12">
          <h1 class="display-7 text-center">Sparsh Partners</h1>

          <!-- ---------------USER DETAILS TABLE ------------- -->

          <div class="row">
          <div class="col-12 ml-auto mr-auto">
          	<div class="ml-auto text-center">
          		<div class="btn-group">
          			<button class="btn btn-info" data-toggle="modal" data-target="#modal2">Add Partner</button>
          		</div>
            </div><br>
            <div class="ml-auto">
              <button class="btn btn-outline-danger " onclick="exportTableToCSV('sparshPartners.csv')">Export Data To CSV File</button>
            </div><br>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Sparsh Partners</h3>
                <div class="card-tools">
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               <div class=" table-responsive p-0">
                <table class="display table table-hover text-nowrap" id="sparshUsers">
                  <thead>
			            <tr>
			                <th>ID</th>
			                <th>Name</th>
                      <th>Code</th>
			                <th>Email</th>
			                <th>Phone</th>
                      <th>Link</th>
                      <th>Reg. date</th>
                      <th>Action</th>
			            </tr>
			        </thead>
			        <tbody id="userListBody">
			        	
			        </tbody>
			        <tfoot>
			            <tr>
			                <th>ID</th>
                      <th>Name</th>
                      <th>Code</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Link</th>
                      <th>Reg. date</th>
                      <th>Action</th>
			            </tr>
			        </tfoot>
                </table>
              </div>

              </div> 
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
          

        </div>
      </div>
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Partner Details</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               <div class="alert alert-warning mx-auto" id="formWarning2" style="display: none;"></div>
                <div class="alert alert-success mx-auto" id="formSuccess2" style="display: none;"></div>
              <form>
                <div class="row">
                  <div class="col-md-4 col-sm-12">
                    <div class="form-group">
                      <label>Partner Name</label>
                      <input type="text" class="form-control" id="partnerNameEdit" placeholder="Partner Name" required>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <div class="form-group">
                      <label>Partner Code</label>
                      <input type="text" class="form-control" id="partnerCodeEdit" placeholder="Unique Code" readonly required>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <div class="form-group">
                      <label>Partner Email</label>
                      <input type="email" class="form-control" id="emailEdit" placeholder="Email" required>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <div class="form-group">
                      <label>Partner Phone</label>
                      <input type="number" class="form-control" id="phoneEdit" placeholder="Phone Number" required>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <div class="form-group">
                      <label>Partner Website</label>
                      <input type="text" class="form-control" id="websiteEdit" placeholder="Website" required>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12 mx-auto">
                    <div class="form-group">
                      <label>Registration Date</label>
                      <input type="date" class="form-control" id="regDateEdit" placeholder="Reg Date" value="<?php echo date("Y-m-d"); ?>" required readonly>
                    </div>
                  </div>
                </div><hr>
                <h3 class="text-center">Session Starting Date</h3>
                <div class="row">
                  <div class="col-md-6 mx-auto col-sm-12">
                  <div class="form-group">
                    <label>PG Session</label>
                    <input type="date" class="form-control" id="pgDate" placeholder="PG Starting Date" required>
                  </div>
                </div>
                <div class="col-md-6 mx-auto col-sm-12">
                  <div class="form-group">
                    <label>IK1 Session</label>
                    <input type="date" class="form-control" id="ik1Date" placeholder="Ik1 Starting Date" required>
                  </div>
                </div>
                <div class="col-md-6 mx-auto col-sm-12">
                  <div class="form-group">
                    <label>IK2 Session</label>
                    <input type="date" class="form-control" id="ik2Date" placeholder="IK2 Starting Date" required>
                  </div>
                </div>
                <div class="col-md-6 mx-auto col-sm-12">
                  <div class="form-group">
                    <label>IK3 Session</label>
                    <input type="date" class="form-control" id="ik3Date" placeholder="Ik3 Starting Date" required>
                  </div>
                </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <p id="saveMsg2" style="display: none; font-weight: bold;">Saving Data...</p>
              <button id="saveBtn2" type="button" onclick="savePartnerData();" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>
</div>

<!-- Add partner modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Partner</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" method="POST">
        	<div class="col-md-8 col-sm-12 mx-auto">
        		<div class="alert alert-warning mx-auto" id="formWarning" style="display: none;"></div>
        		<div class="alert alert-success mx-auto" id="formSuccess" style="display: none;"></div>
        		<div class="row">
        			<div class="col-md-6 col-sm-12">
        				<div class="form-group">
                  <label>Partner Name</label>
        					<input type="text" class="form-control" placeholder="Partner Name" id="partnerName" required>
        				</div>
        			</div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <label>Partner Code</label>
                  <input type="text" class="form-control" placeholder="Unique Code" id="partnerCode" required>
                </div>
              </div>
        			<div class="col-md-6 col-sm-12">
        				<div class="form-group">
                  <label>Partner Email</label>
        					<input type="email" class="form-control" placeholder="Email" id="email" required>
        				</div>
        			</div>
        			<div class="col-md-6 col-sm-12">
        				<div class="form-group">
                  <label>Partner Phone</label>
        					<input type="number" class="form-control" placeholder="Phone Number" id="phone" required>
        				</div>
        			</div>
        			<div class="col-md-6 col-sm-12">
        				<div class="form-group">
                  <label>Partner Website</label>
        					<input type="text" class="form-control" placeholder="Website" id="website" required>
        				</div>
        			</div>
        			<div class="col-md-6 col-sm-12 mx-auto">
        				<div class="form-group">
                  <label>Registration Date</label>
        					<input type="date" class="form-control" placeholder="Reg Date" value="<?php echo date("Y-m-d"); ?>" id="regDate" required>
        				</div>
        			</div>
        		</div>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <p id="saveMsg" style="display: none; font-weight: bold;">Saving Data...</p><button type="button" id="saveBtn" onclick="addPartner();" class="btn btn-primary">Save changes</button> 
      </div>
    </div>
  </div>
</div>

<!-- Vanilla js & Jquery code -->
<script type="text/javascript">
  alert = function() {};
	var partnerName = null;
	var email = null;
	var phone = null;
	var partnerCode = null;
	var regDate = null;
  var website = null;
  var password = null;
	var formWarning = document.getElementById('formWarning');
	var formSuccess = document.getElementById('formSuccess');
	var saveMsg = document.getElementById('saveMsg');
	var saveBtn = document.getElementById('saveBtn');
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

	function selectAge(){
		var e = document.getElementById("childAge");
		childAge = e.options[e.selectedIndex].value;
		console.log(childAge);
	}

	function fetchData() {
		$.ajax({ 
	      url: "/API/V1/?sparshAllPartners",
	      dataType:"html",
	      type: "post",
	      success: function(data){
	        var table = $('#sparshUsers');
	        var body = $('#userListBody');
          // table.remove();
	        table.find("tbody tr").remove();
	        table.find("tbody div").remove();
	        body.append(data);
          // $("#sparshUsers").dataTable().fnDestroy();
	        $('#sparshUsers').DataTable( {
		        "order": [[ 0, "desc" ]],
		        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            // "bDestroy": true
	    	});
	      }
	    });
	}
	
	$(document).ready(fetchData());

	function editData(id){
		
	}
	function generatePassword(length){
      var result           = '';
      var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      return result;
    }

	function addPartner(){
		saveMsg.style.display = "inline-block";
 		saveMsg.innerHTML = 'Saving Data...';
 		saveBtn.disabled = true;
 		formWarning.style.display = 'none';
 		formSuccess.style.display = 'none';
 		partnerName = document.getElementById('partnerName').value; 
 		email = document.getElementById('email').value; 
 		phone = document.getElementById('phone').value; 
 		regDate = document.getElementById('regDate').value; 
 		partnerCode = document.getElementById('partnerCode').value; 
    website = document.getElementById('website').value; 
    password = generatePassword(6);
		if (!partnerName || !partnerCode || !email || !phone || !regDate) {
			formWarning.style.display = 'inline-block';
			formWarning.innerHTML = 'Please fill all the fields';
			saveMsg.style.display = "none";
			saveBtn.disabled = false;
		}else if(reg.test(email) == false){
      formWarning.style.display = 'inline-block';
      formWarning.innerHTML = 'provide a valid email';
      saveMsg.style.display = "none";
      saveBtn.disabled = false;
    }
   else{
			formWarning.style.display = 'none';
		  let formData = new FormData();
		  formData.append("formName", "registerPartner");
	      formData.append("partnerName", partnerName);
	      formData.append("email", email);
	      formData.append("phone", phone);
	      formData.append("partnerCode", partnerCode);
        formData.append("website", website);
	      formData.append("regDate", regDate);
        formData.append("password", password);
	      fetch('/API/V1/', {
	        method: "POST",
	        body: formData
	      }).then(function(response) {
	        if (response.status !== 200) {
	          console.log(
	            "Looks like there was a problem. Status Code: " + response.status
	          );
	          return;
	        }
	        response.json().then(function(data) {
	          console.log(data);
	          saveBtn.disabled = false;
	          saveMsg.style.display = "none";
	          if (data.errorm != null) {
	            formWarning.style.display = 'inline-block';
				      formWarning.innerHTML = data.errorm;
	          } else {
              formWarning.style.display = 'none';
	            formSuccess.style.display = 'inline-block';
				      formSuccess.innerHTML = data.data;
	            // sendMailToPartner();
	            document.getElementById('partnerName').value = null;
      				document.getElementById('email').value = null;
      				document.getElementById('phone').value = null;
      				document.getElementById('regDate').value = null;
              document.getElementById('partnerCode').value = null;
              document.getElementById('website').value = null;
              fetchData();
	          }
	       });
		  });
	  }
  }

  // function sendMailToPartner(){
  //   let formData = new FormData();
  //   formData.append("sendMail", "true");
  //   formData.append("reciever", email);
  //   formData.append("sender", "Sparsh");
  //   formData.append("senderMail", "no-reply@teenybeans.in");
  //   formData.append("subject", "Welcome to Sparsh");
  //   formData.append(
  //     "message",
  //     "<html><body><h2>Welcome to Sparsh - the parent App. Sparsh is an APP connecting parents with Teeny Beans' internationally recognized preschool curriculum. You have been registered as a parent and provided the login credentials for Sparsh.</h2><h2>Sparsh Download Link:- <a href='https://play.google.com/store/apps/details?id=com.beanstalkedu.app.sparsh'>Click here to Download</a></h2><h2>Login Id:- "+email+"</h2><h2>Password:- "+password+"</h2><p>For more information about us please visit <a href='https://teenybeans.in/'>our website</a>.<br>Thank you.<br>-Team Teenybeans.</p></body></html>"
  //   );
  //   fetch("https://mailapi.teenybeans.in/", {
  //     method: "POST",
  //     body: formData
  //   })
  //   .then(function(response) {
  //     response.json().then(function(data) {
  //     });
  //   })
  //   .catch(function(err) {
  //     console.log("Fetch Error :-S", err);
  //   });
  // }

  // EDIT VARIABLES'

  var partnerNameEdit = null;
  var emailEdit = null;
  var phoneEdit = null;
  var partnerCodeEdit = null;
  var regDateEdit = null;
  var websiteEdit = null;
  var formWarning2 = document.getElementById('formWarning2');
  var formSuccess2 = document.getElementById('formSuccess2');
  var saveMsg2 = document.getElementById('saveMsg2');
  var saveBtn2 = document.getElementById('saveBtn2');
  var pgDate = null;
  var ik1Date = null;
  var ik2Date = null;
  var ik3Date = null;

  function fetchSinglePartner(code){
    let self = this;
    fetch('/API/V1/?fetchSinglePartner&partnerCode='+code)
      .then(
        function(response) {
          if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' +
              response.status);
            return;
          }
          // Examine the text in the response
          response.json().then(function(data) {
            if(data){
              // console.log(data.data)
              var sessionDetails = JSON.parse(data.data.DETAILS)
              console.log(sessionDetails);
              document.getElementById('partnerNameEdit').value = data.data.PARTNER_NAME;
              document.getElementById('partnerCodeEdit').value = data.data.PARTNER_CODE;
              document.getElementById('emailEdit').value = data.data.EMAIL;
              document.getElementById('phoneEdit').value = data.data.PHONE;
              document.getElementById('websiteEdit').value = data.data.WEBSITE;
              document.getElementById('regDateEdit').value = data.data.REG_DATE;
              document.getElementById('pgDate').value = sessionDetails.pgDate;
              document.getElementById('ik1Date').value = sessionDetails.ik1Date;
              document.getElementById('ik2Date').value = sessionDetails.ik2Date;
              document.getElementById('ik3Date').value = sessionDetails.ik3Date;
            }else{
              // self.noticeAlert = "No blog present. Please create one!";
              // self.showDismissibleAlert = true;
              // self.pages = null;
            }
            
          });
        }
      )
      .catch(function(err) {
        console.log('Fetch Error :-S', err);
      });
   }

   function savePartnerData(){
      saveMsg2.style.display = "inline-block";
      saveMsg2.innerHTML = 'Saving Data...';
      saveBtn2.disabled = true;
      formWarning2.style.display = 'none';
      formSuccess2.style.display = 'none';
      partnerNameEdit = document.getElementById('partnerNameEdit').value; 
      emailEdit = document.getElementById('emailEdit').value; 
      phoneEdit = document.getElementById('phoneEdit').value; 
      websiteEdit = document.getElementById('websiteEdit').value; 
      partnerCodeEdit = document.getElementById('partnerCodeEdit').value; 
      pgDate = document.getElementById('pgDate').value;
      ik1Date = document.getElementById('ik1Date').value;
      ik2Date = document.getElementById('ik2Date').value;
      ik3Date = document.getElementById('ik3Date').value;
      // console.log(partnerCodeEdit)
      if (!partnerNameEdit || !partnerCodeEdit || !emailEdit || !phoneEdit || !websiteEdit) {
        formWarning2.style.display = 'inline-block';
        formWarning2.innerHTML = 'Please fill all the fields';
        saveMsg2.style.display = "none";
        saveBtn2.disabled = false;
      }else{
        formWarning2.style.display = 'none';
        let formData = new FormData();
        formData.append("formName", "updatePartner");
        formData.append("partnerName", partnerNameEdit);
        formData.append("email", emailEdit);
        formData.append("phone", phoneEdit);
        formData.append("partnerCode", partnerCodeEdit);
        formData.append("website", websiteEdit);
        formData.append("ik1Date", ik1Date);
        formData.append("ik2Date", ik2Date);
        formData.append("pgDate", pgDate);
        formData.append("ik3Date", ik3Date);
        fetch('/API/V1/', {
          method: "POST",
          body: formData
        }).then(function(response) {
          if (response.status !== 200) {
            console.log(
              "Looks like there was a problem. Status Code: " + response.status
            );
            return;
          }
          response.json().then(function(data) {
            console.log(data);
            saveBtn2.disabled = false;
            saveMsg2.style.display = "none";
            if (data.errorm != null) {
              formWarning2.style.display = 'inline-block';
              formWarning2.innerHTML = data.errorm;
            } else {
              formWarning2.style.display = 'none';
              formSuccess2.style.display = 'inline-block';
              formSuccess2.innerHTML = data.data;
              // sendMailToPartner();
              fetchData();
              fetchSinglePartner(partnerCodeEdit);
            }
         });
      });
      }
   }

   function deletePartner(partnerId) {
    // console.log(parentId)
      var result = confirm('Are you sure you want to delete the parent record?');
      if (result) {
        let formData = new FormData();
          formData.append("formName", "deletePartner");
          formData.append("userId", partnerId);
        fetch('/API/V1/', {
            method: "POST",
            body: formData
          }).then(function(response) {
            if (response.status !== 200) {
              console.log(
                "Looks like there was a problem. Status Code: " + response.status
              );
              return;
            }
            response.json().then(function(data) {
              console.log(data);
              fetchData();
           });
        });
      }
   }

</script>


<!-- Vue js code -->




<?php endif; ?>