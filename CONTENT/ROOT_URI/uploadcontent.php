<script type="text/javascript">
  $(document).ready(function(){
    $('#contentbook input').change(function () {
      $('#contentbook p').text(this.files.length + " file(s) selected");
    });
    $('#workbook input').change(function () {
      $('#workbook p').text(this.files.length + " file(s) selected");
    });
    $('#lessonplan input').change(function () {
      $('#lessonplan p').text(this.files.length + " file(s) selected");
    });
});
</script>

<link rel="stylesheet" href="https://unpkg.com/vue-select@3.0.0/dist/vue-select.css">
<div class="container" id="vapp">
	<div class="card shadow mb-3 pb-3">
		<div class="card-body">
      <h3 class="text-center">Upload Content</h3>
      <hr>
			<form class="form">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<label>Class</label>
              <select class="form-control" v-model="selectedClass">
                <option value="">Select Class</option>
                <option>PG</option>
                <option>IK1</option>
                <option>IK2</option>
                <option>IK3</option>
              </select> 
						</div>
					</div>
          <div class="col-md-6 col-sm-12">
            <div class="form-group">
              <label>Day</label>
              <input type="number" @input="checkDay" min="1" class="form-control" v-model="selectedDay" placeholder="Enter the day(in number)">
              <p style="color:red;" v-if="dayExists">The day already exists!</p>
              <!-- <p>{{ selectedDay }}</p> -->
            </div>
          </div>
          <div class="col-md-12 col-sm-12">
            <div class="form-group">
              <label>Content Book</label>
              <div class="drag" id="contentbook">
                <input ref="contentbookImg" type="file" multiple>
                <p>Drag content book files here or click here.</p>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-sm-12">
            <div class="form-group">
              <label>Work Book</label>
              <div class="drag" id="workbook">
                <input ref="workbookImg" type="file" multiple>
                <p>Drag Work book files here or click here.</p>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-sm-12">
            <div class="form-group">
              <label>Lesson Plan</label>
              <div class="drag" id="lessonplan">
                <input ref="lessonplanImg" type="file" multiple>
                <p>Drag Lesson Plan files here or click here.</p>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-sm-12">
            <div class="form-group">
              <label>Videos</label>
              <!-- <input type="" name=""> -->
              <v-select multiple v-model="selectedVideos" :options="fetchedVideos"></v-select>
            </div>
          </div>
          <div class="col-md-12 text-right mr-auto col-sm-12">
            <span v-if="disableButton">Loading... </span><button :disabled="disableButton" @click="saveData" type="button" class="btn btn-success">Save</button>
          </div>
          <div class="col-md-12 text-center mr-auto col-sm-12">
            <p style="color: red;">{{ ErrorMsg }}</p>
            <p style="color: green;">{{ successMsg }}</p>
          </div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
Vue.component('v-select', VueSelect.VueSelect);
const vueApp = new Vue({
  el: '#vapp',
  data: { 
   display: 'redbox',
   showDays: false,
   selectedClass: '',
   selectedDay: '',
   workbookImg: [],
   contentbookImg: [],
   lessonplanImg: [],
   fetchedVideos:[],
   selectedVideos:"",
   successMsg:null,
   showMessage:false,
   ErrorMsg: null,
   showLoader:false,
   disableButton:false,
   fetchedContentBook:null,
   fetchedWorkBook:null,
   fetchedLessonPlan:null,
   showModalLoader:false,
   showModalContent:false,
   downloadLinkUrl: 'https://bscdn.sgp1.digitaloceanspaces.com/',
   dayExists:false,
  },
  methods: { 
    checkDay(){
     var self = this;
     console.log(this.selectedDay);
     fetch('/API/V1/?checkDayExist&day='+this.selectedDay+'&class='+this.selectedClass)
      .then(function(response) {
        if (response.status !== 200) {
          console.log(
            "Looks like there was a problem. Status Code: " + response.status
          );
          return;
        }
        response.json().then(function(data) {
          console.log(data);
          if (data.dayExist == 'true') {
            self.dayExists = true;
            self.disableButton = true;
          }else{
            self.dayExists = false;
            self.disableButton = false;
          }
        });
      })
      .catch(function(err) {
        console.log("Fetch Error :-S", err);
      });
    },
    fetchVideos(){
     var self = this;
     fetch('/API/V1/?getVideosList')
      .then(function(response) {
        if (response.status !== 200) {
          console.log(
            "Looks like there was a problem. Status Code: " + response.status
          );
          return;
        }
        response.json().then(function(data) {
          // console.log(data);
          self.fetchedVideos = data.files;
          // console.log(self.fetchedContentBook);
        });
      })
      .catch(function(err) {
        console.log("Fetch Error :-S", err);
      });
    },
    saveData(){
      this.successMsg = null;
      var self = this;
      
      this.disableButton = true;
      let formData = new FormData();
      // console.log(this.$refs);

      //----------CONTENT BOOK----------------
      if (this.$refs.contentbookImg != null) {
        if (this.$refs.contentbookImg.files.length == 0) {
          this.contentbookImg = null;
        }else{
          for( var i = 0; i < this.$refs.contentbookImg.files.length; i++ ){
            let file = this.$refs.contentbookImg.files[i];
            // console.log(file);
            formData.append('contentbookImg[' + i + ']', file);
          } 
        }
      }

      //----------WORK BOOK----------------

      if (this.$refs.workbookImg != null) {
        if (this.$refs.workbookImg.files.length == 0) {
          this.workbookImg = null;
        }else{
          for( var i = 0; i < this.$refs.workbookImg.files.length; i++ ){
            let file = this.$refs.workbookImg.files[i];
            // console.log(file);
            formData.append('workbookImg[' + i + ']', file);
          } 
        }
      }

      //----------LESSON PLAN----------------

      if (this.$refs.lessonplanImg != null) {
        if (this.$refs.lessonplanImg.files.length == 0) {
          this.lessonplanImg = null;
        }else{
          for( var i = 0; i < this.$refs.lessonplanImg.files.length; i++ ){
            let file = this.$refs.lessonplanImg.files[i];
            // console.log(file);
            formData.append('lessonplanImg[' + i + ']', file);
          } 
        }
      }

      if (this.selectedClass == '' || this.selectedDay == '') {
        this.disableButton = false;
        this.ErrorMsg = "Fill Up All the fields";
      }else{
        this.ErrorMsg = '';
        formData.append('class', this.selectedClass);
        formData.append('day', this.selectedDay);
        formData.append('videos', this.selectedVideos);
        fetch("/API/V1/?uploadImages", {
            method: "POST",
            body:formData,
        }).then(
            function(response) {
            response.json().then(function(data) {
              console.log(data);
              self.ErrorMsg = '';
              self.disableButton = false;
              self.successMsg = data.result;
              self.selectedDay = '';
              self.selectedClass = '';
              self.lessonplanImg = [];
              self.workbookImg = [];
              self.contentbookImg = [];
              self.selectedVideos = "";
              // self.fetchVideos();
            });
          }
        )
        .catch(function(err) {
          console.log('Fetch Error :-S', err);
        });
      }

      // console.log(self.contentbookImg); 
    },
    deleteFile(fileName){
      var self = this;
      let formData = new FormData();
      formData.append('fileName', fileName);
      fetch("/API/V1/?deleteFile", {
          method: "POST",
          body:formData,
      }).then(
          function(response) {
          response.json().then(function(data) {
            console.log(data);
            self.showMessage = true;
            self.showLoader = false;
            self.responseFromServer = data.result;
            self.fetchDayContent();
          });
        }
      )
      .catch(function(err) {
        console.log('Fetch Error :-S', err);
      });
    }
  },
  created(){
    this.fetchVideos();
  },
})
</script>
<style scoped>
  .drag{
    /*position: absolute;
    top: 50%;
    left: 50%;
    margin-top: -100px;
    margin-left: -250px;*/
    /*width: 500px;*/
    height: 200px;
    border: 4px dashed black;
  }
  .drag p{
    width: 100%;
    height: 100%;
    text-align: center;
    line-height: 170px;
    color: black;
    font-family: Arial;
  }
  .drag input{
    position: absolute;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    outline: none;
    opacity: 0;
  }
</style>