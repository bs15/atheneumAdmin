

<?php
$link = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
$link->set_charset("utf8");
if(mysqli_connect_error()){
    die("ERROR: UNABLE TO CONNECT: ".mysqli_connect_error());
} 

if($_SESSION['LoggedIn']){


function httpPost($url,$params)
{
	$postData = '';
	 //create name value pairs seperated by &
	foreach($params as $k => $v) 
	{ 
	  $postData .= $k . '='.$v.'&'; 
	}
	$postData = rtrim($postData, '&');

	$ch = curl_init();  

	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($ch,CURLOPT_HEADER, false); 
	curl_setopt($ch, CURLOPT_POST, count($postData));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    

	$output=curl_exec($ch);

	curl_close($ch);
	return $output;

}
// Coupon Management APIs

if (isset($_POST['formName']) && $_POST['formName']=="addCoupon"){
 	// $data = "Hello";
   	$couponName =  $_POST['couponName'];
   	$couponCode =  $_POST['couponCode'];
   	$startDate =  $_POST['startDate'];
    $endDate =  $_POST['endDate'];
    $discount =  $_POST['discount'];
    $product =  $_POST['product'];
    $domain =  $_POST['domain'];

   	$stmt = $link->prepare("INSERT INTO COUPON_MANAGE (`COUPON_NAME`, `COUPON_CODE`, `DISCOUNT`, `START_DATE`, `END_DATE`, `PRODUCT`, `DOMAIN`) VALUES (?, ?, ?, ?, ?, ?, ?)");
	$stmt->bind_param("sssssss", $couponName, $couponCode, $discount, $startDate, $endDate, $product, $domain);
	  if ($stmt->execute()) {        
	    $errorm = null;
	    $data = "Successfully added a new coupon";  
	  }else{
	    $errorm = "Failed-> ".mysqli_error($link);
	  }

	
 
   	
	$myObj = new stdClass();
	$myObj->data = $data;
	$myObj->errorm = $errorm;
	$myJSON = json_encode($myObj);
	echo $myJSON;

}

if (isset($_GET['couponList'])) {
	$sql = "SELECT * FROM COUPON_MANAGE ORDER BY ID DESC";
	$result = mysqli_query($link,$sql);
	if($result){
		if(mysqli_num_rows($result)>0){
			while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
				if ($row['STATUS'] == 'SITE-WIDE') {
					$status = '<button class="btn btn-sm btn-warning" onclick="removeSiteWide(`'.$row['COUPON_CODE'].'` , `'.$row['DOMAIN'].'`)">Remove SiteWide</button>';
				}else{
					$status = '<button class="btn btn-sm btn-success" onclick="makeSiteWide(`'.$row['COUPON_CODE'].'` , `'.$row['DOMAIN'].'`)">Make SiteWide</button>';
				}
				echo '<tr>
				     <td>'.$row['ID'].'</td>
				     <td>'.$row['COUPON_NAME'].'</td>
				      <td>'.$row['COUPON_CODE'].'</td>
				      <td>'.$row['DISCOUNT'].'</td>
				      <td>'.$row['DOMAIN'].'</td>
				      <td>'.$row['START_DATE'].'</td>
				      <td>'.$row['END_DATE'].'</td>
				      <td>
		            	<div class="btn-group"> 
			            	<button class="btn btn-sm btn-primary" onclick="fetchCouponDetails(`'.$row['COUPON_CODE'].'`, `'.$row['DOMAIN'].'`)" data-toggle="modal" data-target="#exampleModal">Edit</button>
			            	'.$status.'
			            	<button class="btn btn-sm btn-danger" onclick="deleteCoupon(`'.$row['COUPON_CODE'].'`, `'.$row['DOMAIN'].'`)">Delete</button>
		            	</div>
		            </td>';
			}
		}
	}
}

if (isset($_GET['fetchSingleCoupon'])){
  $couponCode = $_GET['couponCode'];
  $domain = $_GET['domain'];
  $sql = "SELECT * FROM COUPON_MANAGE WHERE `COUPON_CODE`= '$couponCode' AND `DOMAIN` = '$domain'";
  $result = mysqli_query($link, $sql);
  if ($result) {
    if(mysqli_num_rows($result)>0){
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      // $data = json_encode($row);
      $data = $row;
      $errorm = null;
    }
  }else{
    $data = null;
    $errorm = "Try AGain!";
  }
  $myObj = new stdClass();
	$myObj->data = $data;
	$myObj->errorm = $errorm;
	$myJSON = json_encode($myObj);
	echo $myJSON;
}

if (isset($_POST['formName']) && $_POST['formName']=="updateCouponDetails"){
 	$couponName =  $_POST['couponName'];
   	$couponCode =  $_POST['couponCode'];
   	$startDate =  $_POST['startDate'];
    $endDate =  $_POST['endDate'];
    $discount =  $_POST['discount'];
    $product =  $_POST['product'];
    $domain =  $_POST['domain'];

   
   	$sql = "UPDATE COUPON_MANAGE SET `COUPON_NAME`= '$couponName', `DISCOUNT` = '$discount', `START_DATE` = '$startDate', `END_DATE` = '$endDate', `PRODUCT` = '$product' WHERE `COUPON_CODE` = '$couponCode' AND `DOMAIN` = '$domain' ";
   	
   	$result = mysqli_query($link, $sql);
	
	if ($result) {
		$errorm = null;
	    $data = "Successfully Updated Coupon";
	}else{
		$errorm = "Failed-> ".mysqli_error($link);
	}

   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}

if (isset($_POST['formName']) && $_POST['formName']=="deleteCoupon"){
 	// $data = "Hello";
    $couponCode =  $_POST['couponCode'];
    $domain =  $_POST['domain'];
    
    // $data = $sessionDate;
   
   	$sql = "DELETE FROM COUPON_MANAGE WHERE COUPON_CODE='$couponCode' AND `DOMAIN` = '$domain'";
   	
   	$result = mysqli_query($link, $sql);
	
	if ($result) {

		$errorm = null;
	    $data = "Successfully deleted";
	}else{
		$errorm = "Failed-> ".mysqli_error($link);
	}

   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}

if (isset($_GET['checkCoupon'])){
  $couponCode = $_GET['couponCode'];
  $couponName = $_GET['couponName'];
  $domain = $_GET['domain'];
  $sql = "SELECT * FROM COUPON_MANAGE WHERE (`COUPON_CODE`= '$couponCode' AND `DOMAIN`= '$domain') OR (`COUPON_NAME`= '$couponName' AND `DOMAIN`= '$domain')";
  $result = mysqli_query($link, $sql);
  if ($result) {
    if(mysqli_num_rows($result)>0){
      $data = "present";
      $errorm = null;
    }else{
    	$data = "not present";
    }
  }else{
    $data = null;
    $errorm = "Try Again!";
  }
  $myObj = new stdClass();
	$myObj->data = $data;
	$myObj->errorm = $errorm;
	$myJSON = json_encode($myObj);
	echo $myJSON;
}

if (isset($_POST['formName']) && $_POST['formName']=="makeSiteWide"){
 	// $data = "Hello";
    $couponCode =  $_POST['couponCode'];
    $domain =  $_POST['domain'];
    
    $sqlU = "UPDATE COUPON_MANAGE SET `STATUS`= NULL WHERE DOMAIN = '$domain'";
    $resultU = mysqli_query($link, $sqlU);

    if ($resultU) {
    	$sql = "UPDATE COUPON_MANAGE SET `STATUS`= 'SITE-WIDE' WHERE DOMAIN = '$domain' AND COUPON_CODE = '$couponCode'";
   	
	   	$result = mysqli_query($link, $sql);
		
		if ($result) {

			$errorm = null;
		    $data = "Successfully updated";
		}else{
			$errorm = "Failed-> ".mysqli_error($link);
		}	
    }

   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}

if (isset($_POST['formName']) && $_POST['formName']=="removeSiteWide"){
 	// $data = "Hello";
    $couponCode =  $_POST['couponCode'];
    $domain =  $_POST['domain'];

   	$sql = "UPDATE COUPON_MANAGE SET `STATUS`= '' WHERE DOMAIN = '$domain' AND COUPON_CODE = '$couponCode'";
   	
   	$result = mysqli_query($link, $sql);
	
	if ($result) {

		$errorm = null;
	    $data = "Successfully updated";
	}else{
		$errorm = "Failed-> ".mysqli_error($link);
	}

   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}


// Coupon ends


if (isset($_GET['userList'])) {
	$sql = "SELECT * FROM ATHENEUM_STUDENT ORDER BY ID DESC";
	$result = mysqli_query($link,$sql);
	if($result){
		if(mysqli_num_rows($result)>0){
			while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
				$paid = $row['PAID'];
				$partnerId = $row["PARTNER_ID"];
				if (!$partnerId) {
				 	$partnerId = "NULL";
				 } 
		      	if ($paid == 0) {
		      		$status = '<span style="background-color:#FAD02E; font-weight:bold; padding:5px; cursor:pointer;" title="Payment not confirmed!">Registered</span>';
		      	}elseif ($paid > 0) {
		      		$status = '<span style="background-color:#45CE30; font-weight:bold; padding:5px; cursor:pointer; " title="Successfully Enrolled!">Enrolled</span>';
		      	}
				echo '<tr>
				     <td>'.$row['ID'].'</td>
				     <td><a href="singleUser?id='.$row['UNI_ID'].'">'.$row['NAME'].'</a></td>
				     <td>'.$row['EMAIL'].'</td>
				      <td>'.$row['PHONE'].'</td>
				      <td>'.$row['ENROLL_DATE'].'</td>
				      <td>'.$status.'</td>
				      <td>'.$partnerId.'</td>'
				      ;
				echo ' <td><a href="removeUser?id='.$row['UNI_ID'].'" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';	  

			}

		}else{
			echo '<div class="alert alert-warning">No Data</div>';
		}

	}else{
		echo '<div class="alert alert-danger">Error Running the Query</div>';
		echo '<div class="alert alert-danger">' . mysqli_error($link) . '</div>';
	}
}

// --------- GET ALL THE SPARSH USERS -----------------------
if (isset($_GET['sparshAllUsers'])) {
      // Data Sanitazitation and details in json

	 $sql = "SELECT * FROM PARENT_APP_USERS WHERE ROLE ='PARENT' ORDER BY ID DESC";
	 $result = mysqli_query($link,$sql);
	 $tempArray = [];
	 if ($result) {
	    if(mysqli_num_rows($result)>0){
	      while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) { ?>
	      	<tr>
	            <td><?php echo $row['ID']; ?></td>
	            <td><?php echo $row['PARENT_NAME']; ?></td>
	            <td><?php echo $row['EMAIL']; ?></td>
	            <td><?php echo $row['PHONE']; ?></td>
	            <td><?php echo $row['CHILD_NAME']; ?></td>
	            <td><?php echo $row['AGE']; ?></td>
	            <td><?php echo $row['COURSE_NAME']; ?></td>
	            <td><?php echo $row['CENTER_CODE']; ?></td>
	            <td><?php echo $row['REG_DATE']; ?></td>
	            <td><?php echo $row['STATUS']; ?></td>
	            <td>
	            	<div class="btn-group"> 
		            	<button class="btn btn-sm btn-primary" onclick="fetchSingleParent(`<?php echo $row['USER_ID']; ?>`)" data-toggle="modal" data-target="#exampleModal">Edit</button>
		            	<button class="btn btn-sm btn-danger" onclick="deleteParent(`<?php echo $row['USER_ID']; ?>`)">Delete</button>
	            	</div>
	        	</td>
	        </tr>

	     <?php }
	    }else{
	      // $errorm = "No blog avaliable. Please create one!";
	      // $errorm =json_encode($errorm);
	      $data = null;
	      // $data = json_encode($data);
	    }
	  }else{
	    $errorm = mysqli_error($link);
	  }

}

if (isset($_GET['sparshAllTeachers'])) {
      // Data Sanitazitation and details in json

	 $sql = "SELECT * FROM PARENT_APP_USERS WHERE ROLE ='TEACHER' ORDER BY ID DESC";
	 $result = mysqli_query($link,$sql);
	 $tempArray = [];
	 if ($result) {
	    if(mysqli_num_rows($result)>0){
	      while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) { ?>
	      	<tr>
	            <td><?php echo $row['ID']; ?></td>
	            <td><?php echo $row['PARENT_NAME']; ?></td>
	            <td><?php echo $row['EMAIL']; ?></td>
	            <td><?php echo $row['PHONE']; ?></td>
	            <td><?php echo $row['COURSE_NAME']; ?></td>
	            <td><?php echo $row['CENTER_CODE']; ?></td>
	            <td><?php echo $row['REG_DATE']; ?></td>
	            <td><?php echo $row['STATUS']; ?></td>
	            <td>
	            	<div class="btn-group"> 
		            	<button class="btn btn-sm btn-primary" onclick="fetchSingleTeacher(`<?php echo $row['USER_ID']; ?>`)" data-toggle="modal" data-target="#exampleModal">Edit</button>
		            	<button class="btn btn-sm btn-danger" onclick="deleteTeacher(`<?php echo $row['USER_ID']; ?>`)">Delete</button>
	            	</div>
	            </td>
	        </tr>

	     <?php }
	    }else{
	      // $errorm = "No blog avaliable. Please create one!";
	      // $errorm =json_encode($errorm);
	      $data = null;
	      // $data = json_encode($data);
	    }
	  }else{
	    $errorm = mysqli_error($link);
	  }

}
if (isset($_GET['sparshAllPartners'])) {
      // Data Sanitazitation and details in json

	 $sql = "SELECT * FROM SPARSH_PARTNERS ORDER BY ID DESC";
	 $result = mysqli_query($link,$sql);
	 $tempArray = [];
	 if ($result) {
	    if(mysqli_num_rows($result)>0){
	      while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) { ?>
	      	<tr>
	            <td><?php echo $row['ID']; ?></td>
	            <td><?php echo $row['PARTNER_NAME']; ?></td>
	            <td><?php echo $row['PARTNER_CODE']; ?></td>
	            <td><?php echo $row['EMAIL']; ?></td>
	            <td><?php echo $row['PHONE']; ?></td>
				<td><?php echo $row['WEBSITE']; ?></td>
	            <td><?php echo $row['REG_DATE']; ?></td>
	            <td>
	            	<div class="btn-group"> 
		            	<button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal" onclick="fetchSinglePartner(`<?php echo $row['PARTNER_CODE']; ?>`);">Edit</button>
		            	<button class="btn btn-sm btn-danger" onclick="deletePartner(`<?php echo $row['UNI_ID']; ?>`);">Delete</button>
	            	</div>
	            </td>
	        </tr>

	     <?php }
	    }else{
	      $errorm = "No Record Available";
	      // $errorm =json_encode($errorm);
	      $data = null;
	      // $data = json_encode($data);
	    }
	  }else{
	    $errorm = mysqli_error($link);
	  }

}

// register parent


if (isset($_POST['formName']) && $_POST['formName']=="registerParentAdmin"){
 	// $data = "Hello";
   	$parentName =  $_POST['parentName'];
   	$childAge =  $_POST['childAge'];
   	$childName =  $_POST['childName'];
   	$phone =  $_POST['phone'];
   	$email =  $_POST['email'];
   	$courseName =  $_POST['courseName'];
    $partnerCode =  $_POST['partnerCode'];
    $password =  $_POST['password'];
    $regDate =  $_POST['regDate'];

    $sqlpartners = "SELECT * FROM SPARSH_PARTNERS WHERE PARTNER_CODE = '$partnerCode'";
	$resultpartners = mysqli_query($link, $sqlpartners);
	if ($resultpartners){
		if(mysqli_num_rows($resultpartners)>0){
			$rowpartners = mysqli_fetch_array($resultpartners,MYSQLI_ASSOC);
			$details = $rowpartners['DETAILS'];
		    $details = json_decode($details);
		    if ($courseName == 'PG') {
		    	$sessionDate = $details->{'pgDate'};
		    }else if ($courseName == 'IK1') {
		    	$sessionDate = $details->{'ik1Date'};
		    }else if ($courseName == 'IK2') {
		    	$sessionDate = $details->{'ik2Date'};
		    }else if ($courseName == 'IK3') {
		    	$sessionDate = $details->{'ik3Date'};
		    }
			$aboutLink = $rowpartners['WEBSITE'];
		}else{
			$aboutLink = "https://teenybeans.in/";
		}
	}


   	
   	$userId = D_create_UserId(); 

   	$hashedPassword = md5($password);
	  $sql = "SELECT * FROM PARENT_APP_USERS WHERE EMAIL = '$email' OR PHONE = '$phone'";
	  $result = mysqli_query($link, $sql);
	  if ($result) {
	    // $data = "success";
	    if(mysqli_num_rows($result)>0){
	      $errorm = "the parent is already registered!";
	    }else{
	      // $data = "Success";
	      $stmt = $link->prepare("INSERT INTO PARENT_APP_USERS (`USER_ID`, `PARENT_NAME`, `EMAIL`, `PHONE`, `PASSWORD`, `CHILD_NAME`, `AGE`, `CENTER_CODE`, `COURSE_NAME`, `REG_DATE`, `ABOUT_LINK`, `STATUS`, `ROLE`, `SESSION_DATE`, `INTERAKTO`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 'APPROVED', 'PARENT', ?, '1')");
	          $stmt->bind_param("ssssssssssss", $userId, $parentName, $email, $phone, $hashedPassword, $childName, $childAge, $partnerCode, $courseName, $regDate, $aboutLink, $sessionDate);
	      if ($stmt->execute()) {        
	        $errorm = null;
	        $data = "Successfully registered parent";  
	      }else{
	        $errorm = "Failed-> ".mysqli_error($link);
	      }
	    }
	    
	  }
   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}

// REGISTER TEACHER

if (isset($_POST['formName']) && $_POST['formName']=="registerTeacher"){
 	// $data = "Hello";
   	$teacherName =  $_POST['teacherName'];
   	$phone =  $_POST['phone'];
   	$email =  $_POST['email'];
   	$selcetedClass =  $_POST['selcetedClass'];
    $partnerCode =  $_POST['partnerCode'];
    $password =  $_POST['password'];
    $regDate =  $_POST['regDate'];

    $sqlpartners = "SELECT * FROM SPARSH_PARTNERS WHERE PARTNER_CODE = '$partnerCode'";
	$resultpartners = mysqli_query($link, $sqlpartners);
	if ($resultpartners){
		if(mysqli_num_rows($resultpartners)>0){
			$rowpartners = mysqli_fetch_array($resultpartners,MYSQLI_ASSOC);
			$details = $rowpartners['DETAILS'];
		    $details = json_decode($details);
		    if ($selcetedClass == 'PG') {
		    	$sessionDate = $details->{'pgDate'};
		    }else if ($selcetedClass == 'IK1') {
		    	$sessionDate = $details->{'ik1Date'};
		    }else if ($selcetedClass == 'IK2') {
		    	$sessionDate = $details->{'ik2Date'};
		    }else if ($selcetedClass == 'IK3') {
		    	$sessionDate = $details->{'ik3Date'};
		    }
			$aboutLink = $rowpartners['WEBSITE'];
		}else{
			$aboutLink = "https://teenybeans.in/";
		}
	}

   	
   	$userId = D_create_UserId(); 

   	$hashedPassword = md5($password);
	  $sql = "SELECT * FROM PARENT_APP_USERS WHERE EMAIL = '$email' OR PHONE = '$phone'";
	  $result = mysqli_query($link, $sql);
	  if ($result) {
	    // $data = "success";
	    if(mysqli_num_rows($result)>0){
	      $errorm = "the email is already registered!";
	    }else{
	      // $data = "Success";
	      $stmt = $link->prepare("INSERT INTO PARENT_APP_USERS (`USER_ID`, `PARENT_NAME`, `EMAIL`, `PHONE`, `PASSWORD`, `CENTER_CODE`, `COURSE_NAME`, `REG_DATE`, `ABOUT_LINK`, `STATUS`, `ROLE`, `SESSION_DATE`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 'APPROVED', 'TEACHER', ?)");
	          $stmt->bind_param("ssssssssss", $userId, $teacherName, $email, $phone, $hashedPassword, $partnerCode, $selcetedClass, $regDate, $aboutLink, $sessionDate);
	      if ($stmt->execute()) {        
	        $errorm = null;
	        $data = "Succesfully registered teacher";  
	      }else{
	        $errorm = "Failed-> ".mysqli_error($link);
	      }
	    }
	    
	  }
   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}

if (isset($_POST['formName']) && $_POST['formName']=="registerPartner"){
 	// $data = "Hello";
   	$partnerName =  $_POST['partnerName'];
   	$phone =  $_POST['phone'];
   	$email =  $_POST['email'];
    $partnerCode =  $_POST['partnerCode'];
    $regDate =  $_POST['regDate'];
    $website =  $_POST['website'];
    $password =  $_POST['password'];


   	
   	$userId = D_create_UserId(); 
   	$password = md5($password);

	  $sql = "SELECT * FROM SPARSH_PARTNERS WHERE EMAIL = '$email' OR PHONE = '$phone'";
	  $result = mysqli_query($link, $sql);
	  if ($result) {
	  	$sqlfetchHolidays = "SELECT * FROM SPARSH_CENTRAL WHERE ATTRIBUTE = 'HOLIDAYS'";
	  	$resultFetchHolidays = mysqli_query($link, $sqlfetchHolidays);
	  	if ($resultFetchHolidays) {
	  		if(mysqli_num_rows($resultFetchHolidays)>0){
      			$rowHolidays = mysqli_fetch_array($resultFetchHolidays,MYSQLI_ASSOC);
      			$holidays = $rowHolidays['CONTENT_JSON'];
      		}
	  	}
	    // $data = "success";
	    if(mysqli_num_rows($result)>0){
	      $errorm = "the partner is already registered!";
	    }else{
	      // $data = "Success";
	      $stmt = $link->prepare("INSERT INTO SPARSH_PARTNERS (`UNI_ID`, `PARTNER_NAME`, `EMAIL`, `PHONE`, `PARTNER_CODE`, `REG_DATE`, `WEBSITE`, `PASSWORD`,`HOLIDAY_DETAILS`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
	      $stmt->bind_param("sssssssss", $userId, $partnerName, $email, $phone, $partnerCode, $regDate, $website, $password, $holidays);
	      if ($stmt->execute()) {        
	        $errorm = null;
	        $data = "Successfully registered partner";  
	      }else{
	        $errorm = "Failed-> ".mysqli_error($link);
	      }
	    }
	    
	  }
   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}

if (isset($_GET['fetchSinglePartner'])){
  $partnerCode = $_GET['partnerCode'];
  $sql = "SELECT * FROM SPARSH_PARTNERS WHERE `PARTNER_CODE`= '$partnerCode'";
  $result = mysqli_query($link, $sql);
  if ($result) {
    if(mysqli_num_rows($result)>0){
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      // $data = json_encode($row);
      $data = $row;
      $errorm = null;
    }
  }else{
    $data = null;
    $errorm = "Try AGain!";
  }
  $myObj = new stdClass();
	$myObj->data = $data;
	$myObj->errorm = $errorm;
	$myJSON = json_encode($myObj);
	echo $myJSON;
}

if (isset($_POST['formName']) && $_POST['formName']=="updatePartner"){
 	// $data = "Hello";
   	$partnerName =  $_POST['partnerName'];
   	$phone =  $_POST['phone'];
   	$email =  $_POST['email'];
    $partnerCode =  $_POST['partnerCode'];
    $website =  $_POST['website'];
    $pgDate =  $_POST['pgDate'];
    $ik1Date =  $_POST['ik1Date'];
    $ik2Date =  $_POST['ik2Date'];
    $ik3Date =  $_POST['ik3Date'];

    $sessionDetails = array("pgDate" => $pgDate, "ik1Date" => $ik1Date, "ik2Date" => $ik2Date, "ik3Date" => $ik3Date);
    $sessionDetails =  json_encode($sessionDetails);
   
   	$sql = "UPDATE SPARSH_PARTNERS SET `PARTNER_NAME`= '$partnerName', `EMAIL` = '$email', `PHONE` = '$phone', `WEBSITE` = '$website', `DETAILS`='$sessionDetails' WHERE PARTNER_CODE = '$partnerCode' ";
   	
   	$result = mysqli_query($link, $sql);
	
	if ($result) {
		$sqlUserUpdate1 = "UPDATE PARENT_APP_USERS SET `SESSION_DATE` = '$ik1Date' WHERE `CENTER_CODE` = '$partnerCode' AND `COURSE_NAME` = 'IK1' ";
		$resultUserUpdate1 = mysqli_query($link, $sqlUserUpdate1);

		$sqlUserUpdate2 = "UPDATE PARENT_APP_USERS SET `SESSION_DATE` = '$ik2Date' WHERE `CENTER_CODE` = '$partnerCode' AND `COURSE_NAME` = 'IK2' ";
		$resultUserUpdate2 = mysqli_query($link, $sqlUserUpdate2);

		$sqlUserUpdate3 = "UPDATE PARENT_APP_USERS SET `SESSION_DATE` = '$ik3Date' WHERE `CENTER_CODE` = '$partnerCode' AND `COURSE_NAME` = 'IK3' ";
		$resultUserUpdate3 = mysqli_query($link, $sqlUserUpdate3);

		$sqlUserUpdate4 = "UPDATE PARENT_APP_USERS SET `SESSION_DATE` = '$pgDate' WHERE `CENTER_CODE` = '$partnerCode' AND `COURSE_NAME` = 'PG' ";
		$resultUserUpdate4 = mysqli_query($link, $sqlUserUpdate4);
		if (!$resultUserUpdate4) {
			$errorm = mysqli_error($link);
		}


		$errorm = null;
	    $data = "Successfully Updated partner";
	}else{
		$errorm = "Failed-> ".mysqli_error($link);
	}

   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}

if (isset($_GET['fetchSingleParent'])){
  $id = $_GET['id'];
  $sql = "SELECT * FROM PARENT_APP_USERS WHERE `USER_ID`= '$id'";
  $result = mysqli_query($link, $sql);
  if ($result) {
    if(mysqli_num_rows($result)>0){
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      // $data = json_encode($row);
      $data = $row;
      $errorm = null;
    }
  }else{
    $data = null;
    $errorm = "Try AGain!";
  }
  $myObj = new stdClass();
	$myObj->data = $data;
	$myObj->errorm = $errorm;
	$myJSON = json_encode($myObj);
	echo $myJSON;
}

if (isset($_POST['formName']) && $_POST['formName']=="updateParents"){
 	// $data = "Hello";
   	$parentId =  $_POST['parentId'];
   	$parentName =  $_POST['parentName'];
   	$parentId =  $_POST['parentId'];
   	$phone =  $_POST['phone'];
   	$email =  $_POST['email'];
   	$childName =  $_POST['childName'];
   	$age =  $_POST['age'];
    $partnerCode =  $_POST['partnerCode'];
    $partnerCode = strtoupper($partnerCode);
    $partnerCode = preg_replace('/\s+/', '', $partnerCode);
    $class =  $_POST['class'];
    $class = strtoupper($class);
    $sessionDate =  $_POST['sessionDate'];

    if (!$sessionDate || $sessionDate == '') {
    	$sqlFetchSession = "SELECT * FROM SPARSH_PARTNERS WHERE PARTNER_CODE = '$partnerCode'";
	    $resultFetchSession = mysqli_query($link, $sqlFetchSession);
	    $row = mysqli_fetch_array($resultFetchSession,MYSQLI_ASSOC);

	    $details = $row['DETAILS'];
	    $details = json_decode($details);
	    if ($class == 'PG') {
	    	$sessionDate = $details->{'pgDate'};
	    }else if ($class == 'IK1') {
	    	$sessionDate = $details->{'ik1Date'};
	    }else if ($class == 'IK2') {
	    	$sessionDate = $details->{'ik2Date'};
	    }else if ($class == 'IK3') {
	    	$sessionDate = $details->{'ik3Date'};
	    }
	
    }
    
    // $data = $sessionDate;
   
   	$sql = "UPDATE PARENT_APP_USERS SET `PARENT_NAME`= '$parentName', `EMAIL` = '$email', `PHONE` = '$phone', `CHILD_NAME` = '$childName', `AGE` = '$age', `COURSE_NAME` = '$class', `CENTER_CODE` = '$partnerCode', `SESSION_DATE`='$sessionDate' WHERE USER_ID = '$parentId' ";
   	
   	$result = mysqli_query($link, $sql);
	
	if ($result) {

		$errorm = null;
	    $data = "Successfully Updated parent";
	}else{
		$errorm = "Failed-> ".mysqli_error($link);
	}

   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}

if (isset($_POST['formName']) && $_POST['formName']=="updateTeacher"){
 	// $data = "Hello";
   	$teacherId =  $_POST['teacherId'];
   	$teacherName =  $_POST['teacherName'];
   	$phone =  $_POST['phone'];
   	$email =  $_POST['email'];
    $partnerCode =  $_POST['partnerCode'];
    $partnerCode = strtoupper($partnerCode);
    $partnerCode = preg_replace('/\s+/', '', $partnerCode);
    $class =  $_POST['class'];
    $class = strtoupper($class);
    $sessionDate =  $_POST['sessionDate'];

    if (!$sessionDate || $sessionDate == '') {
    	$sqlFetchSession = "SELECT * FROM SPARSH_PARTNERS WHERE PARTNER_CODE = '$partnerCode'";
	    $resultFetchSession = mysqli_query($link, $sqlFetchSession);
	    $row = mysqli_fetch_array($resultFetchSession,MYSQLI_ASSOC);

	    $details = $row['DETAILS'];
	    $details = json_decode($details);
	    if ($class == 'PG') {
	    	$sessionDate = $details->{'pgDate'};
	    }else if ($class == 'IK1') {
	    	$sessionDate = $details->{'ik1Date'};
	    }else if ($class == 'IK2') {
	    	$sessionDate = $details->{'ik2Date'};
	    }else if ($class == 'IK3') {
	    	$sessionDate = $details->{'ik3Date'};
	    }
	
    }
    
    // $data = $sessionDate;
   
   	$sql = "UPDATE PARENT_APP_USERS SET `PARENT_NAME`= '$teacherName', `EMAIL` = '$email', `PHONE` = '$phone', `COURSE_NAME` = '$class', `CENTER_CODE` = '$partnerCode', `SESSION_DATE`='$sessionDate' WHERE USER_ID = '$teacherId' ";
   	
   	$result = mysqli_query($link, $sql);
	
	if ($result) {

		$errorm = null;
	    $data = "Successfully Updated Teacher";
	}else{
		$errorm = "Failed-> ".mysqli_error($link);
	}

   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}

if (isset($_POST['formName']) && $_POST['formName']=="changeStatus"){
 	// $data = "Hello";
    $parentId =  $_POST['parentId'];

    $sqlFetchSession = "SELECT * FROM PARENT_APP_USERS WHERE USER_ID = '$parentId'";
    $resultFetchSession = mysqli_query($link, $sqlFetchSession);
    $row = mysqli_fetch_array($resultFetchSession,MYSQLI_ASSOC);

    $status = $row['STATUS'];
    if ($status == 'APPROVED') {
    	$newStatus = 'PENDING'; 
    }else{
    	$newStatus = 'APPROVED';
    }

    
    // $data = $sessionDate;
   
   	$sql = "UPDATE PARENT_APP_USERS SET `STATUS`= '$newStatus' WHERE USER_ID = '$parentId' ";
   	
   	$result = mysqli_query($link, $sql);
	
	if ($result) {

		$errorm = null;
	    $data = "Successfully Updated status";
	}else{
		$errorm = "Failed-> ".mysqli_error($link);
	}

   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}

if (isset($_POST['formName']) && $_POST['formName']=="deleteUser"){
 	// $data = "Hello";
    $userId =  $_POST['userId'];
    
    // $data = $sessionDate;
   
   	$sql = "DELETE FROM PARENT_APP_USERS WHERE USER_ID='$userId'";
   	
   	$result = mysqli_query($link, $sql);
	
	if ($result) {

		$errorm = null;
	    $data = $userId;
	}else{
		$errorm = "Failed-> ".mysqli_error($link);
	}

   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}

if (isset($_POST['formName']) && $_POST['formName']=="deletePartner"){
 	// $data = "Hello";
    $userId =  $_POST['userId'];
    
    // $data = $sessionDate;
   
   	$sql = "DELETE FROM SPARSH_PARTNERS WHERE UNI_ID='$userId'";
   	
   	$result = mysqli_query($link, $sql);
	
	if ($result) {

		$errorm = null;
	    $data = $userId;
	}else{
		$errorm = "Failed-> ".mysqli_error($link);
	}

   	// $data = $password;
   	
	$myObj = new stdClass();
	$myObj->data = $data;
	$myObj->errorm = $errorm;
	$myJSON = json_encode($myObj);
	echo $myJSON;

}

//ATHENEUM PARTNERS

if (isset($_GET['atheneumPartners'])) {
      // Data Sanitazitation and details in json

	 $sql = "SELECT * FROM ATHENEUM_PARTNERS ORDER BY ID DESC";
	 $result = mysqli_query($link,$sql);
	 $tempArray = [];
	 if ($result) {
	    if(mysqli_num_rows($result)>0){
	      while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) { ?>
	      	<tr>
	            <td><?php echo $row['ID']; ?></td>
	            <td><?php echo $row['NAME']; ?></td>
	            <td><?php echo $row['PARTNER_ID']; ?></td>
	            <td><?php echo $row['EMAIL']; ?></td>
	            <td><?php echo $row['PHONE']; ?></td>
	            <td><?php echo $row['REG_DATE']; ?></td>
	            <td>
	            	<div class="btn-group"> 
		            	<button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal" onclick="fetchSinglePartner(`<?php echo $row['UNI_ID']; ?>`);">Edit</button>
		            	<button class="btn btn-sm btn-danger" onclick="deletePartner(`<?php echo $row['UNI_ID']; ?>`);">Delete</button>
	            	</div>
	            </td>
	        </tr>

	     <?php }
	    }else{
	      $errorm = "No Record Available";
	      // $errorm =json_encode($errorm);
	      $data = null;
	      // $data = json_encode($data);
	    }
	  }else{
	    $errorm = mysqli_error($link);
	  }

}

if (isset($_POST['formName']) && $_POST['formName']=="registerAtheneumPartner"){
 	// $data = "Hello";
   	$partnerName =  $_POST['partnerName'];
   	$phone =  $_POST['phone'];
   	$password =  $_POST['password'];
   	$password = md5($password);
   	$email =  $_POST['email'];
    $partnerCode =  $_POST['partnerCode'];
    $partnerCode = strtoupper($partnerCode);
    $partnerCode = preg_replace('/\s+/', '', $partnerCode);
    $regDate =  $_POST['regDate'];
    $commission =  $_POST['commission'];


   	
   	$userId = D_create_UserId(); 

	  $sql = "SELECT * FROM ATHENEUM_PARTNERS WHERE EMAIL = '$email' OR PHONE = '$phone' OR PARTNER_ID = '$partnerCode'";
	  $result = mysqli_query($link, $sql);
	  if ($result) {
	    // $data = "success";
	    if(mysqli_num_rows($result)>0){
	      $errorm = "the partner is already registered!";
	    }else{
	      // $data = "Success";
	      $stmt = $link->prepare("INSERT INTO ATHENEUM_PARTNERS (`UNI_ID`, `NAME`, `EMAIL`, `PHONE`, `PARTNER_ID`, `REG_DATE`, `PASSWORD`, `COMMISSION`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
	      $stmt->bind_param("ssssssss", $userId, $partnerName, $email, $phone, $partnerCode, $regDate, $password, $commission);
	      if ($stmt->execute()) {        
	        $errorm = null;
	        $data = "Successfully registered partner";  
	      }else{
	        $errorm = "Failed-> ".mysqli_error($link);
	      }
	    }
	    
	  }
   	// $data = $password;
   	
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;

}

if (isset($_GET['fetchSingleAtheneumPartner'])){
  $partnerCode = $_GET['partnerCode'];
  $sql = "SELECT * FROM ATHENEUM_PARTNERS WHERE `UNI_ID`= '$partnerCode'";
  $result = mysqli_query($link, $sql);
  if ($result) {
    if(mysqli_num_rows($result)>0){
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      // $data = json_encode($row);
      $data = $row;
      $errorm = null;
    }
  }else{
    $data = null;
    $errorm = "Try AGain!";
  }
  $myObj = new stdClass();
  $myObj->data = $data;
  $myObj->errorm = $errorm;
  $myJSON = json_encode($myObj);
  echo $myJSON;
}
if (isset($_GET['fetchInvoices'])){
  $partnerCode = $_GET['partnerCode'];
  $sql = "SELECT * FROM ATHENEUM_INVOICES WHERE `USER_ID`= '$partnerCode' ORDER BY STATUS";
  $result = mysqli_query($link, $sql);
  if ($result) {
    if(mysqli_num_rows($result)>0){
    	$data = '<table class="table">
			  <thead>
			    <tr>
			      <th>ID</th>
			      <th>Amount</th>
			      <th>Date</th>
			      <th>Status</th>
			      <th>Action</th>
			    </tr>
			  </thead>
			  <tbody>';
      while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
      	$data .= '<tr>
		      <td>'.$row["INVOICE_ID"].'</td>
		      <td>'.$row["CURRENCY"].' '.$row["AMOUNT"].'</td>
		      <td>'.$row["DATE_RAISED"].'</td>
		      <td>'.$row["STATUS"].'</td>';
		      if ($row["STATUS"] == 'RAISED') {
		      	$data .= '<td><button class="btn btn-success" onclick="processInvoice(`'.$row["INVOICE_ID"].'`)">Accept</button></td>';
		      }else{
		      	$data .= '<td><button class="btn btn-success" disabled>Accept</button></td>';
		      }
		    $data .= '</tr>';
      }
      $data .= '</tbody>
</table>';
$errorm = null;
    }
  }else{
    $data = null;
    $errorm = "Try AGain!";
  }
  $myObj = new stdClass();
  $myObj->data = $data;
  $myObj->errorm = $errorm;
  $myJSON = json_encode($myObj);
  echo $myJSON;
}

if (isset($_GET['processInvoice'])){
  $invoiceId = $_GET['invoiceId'];
  $sql = "SELECT * FROM ATHENEUM_INVOICES WHERE `INVOICE_ID`= '$invoiceId'";
  $result = mysqli_query($link, $sql);
  if ($result) {
    if(mysqli_num_rows($result)>0){
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $partnerId = $row['USER_ID'];
      $curr = $row['CURRENCY'];
      $amount = $row['AMOUNT'];

      $sqlFetchPartner = "SELECT * FROM ATHENEUM_PARTNERS WHERE PARTNER_ID = '$partnerId'";
      $resultFetchPartner = mysqli_query($link, $sqlFetchPartner);
      $rowPartner = mysqli_fetch_array($resultFetchPartner,MYSQLI_ASSOC);

      $earnedMoney = $rowPartner['EARNED_MONEY'];
      if ($earnedMoney) {
		$earnedMoney = json_decode($earnedMoney);
		$indianMoney = $earnedMoney->inr;
		if ($indianMoney) {
			$indianMoney = (int) $indianMoney;
		}else{
			$indianMoney = 0;
		}
		$dollarMoney = $earnedMoney->dollar;
		if ($dollarMoney) {
			$dollarMoney = (int) $dollarMoney;
		}else{
			$dollarMoney = 0;
		}
		if ($curr == "INR") {
			$indianMoney = $amount+$indianMoney;
		}else{
			$dollarMoney = $amount+$dollarMoney;
		}
		$partnerEarning = array('inr' => $indianMoney, 'dollar' => $dollarMoney);
	}else{
		if ($curr == "INR") {
			$partnerEarning = array('inr' => $amount, 'dollar' => 0);
		}else{
			$partnerEarning = array('inr' => 0, 'dollar' => $amount);
		}
	}
	$partnerEarning = json_encode($partnerEarning);
	$sqlUpdatePartner = "UPDATE ATHENEUM_PARTNERS SET EARNED_MONEY = '$partnerEarning' WHERE PARTNER_ID = '$partnerId'";
	$resultUpdatePartner =  mysqli_query($link, $sqlUpdatePartner);
	if ($resultUpdatePartner) {
		$today = date("Y-m-d");
		$sqlUpdateInvoice = "UPDATE ATHENEUM_INVOICES SET STATUS = 'PROCESSED', `DATE_PROCESSED` = '$today' WHERE INVOICE_ID = '$invoiceId'";
		$resultUpdateInvoice =  mysqli_query($link, $sqlUpdateInvoice);
		$sqlNoti = "INSERT INTO ATHENEUM_NOTIFICATION (`USER_ID`, `USER_TYPE`, `TITLE`, `MESSAGE`, `LINK`, `STATUS`) VALUES ('$partnerId', 'PARTNER', 'INVOICE PROCESSED', 'Your invoice has been processed and money has been transfered to your account', 'rewards', 'PENDING')";
        					$resultNoti = mysqli_query($link, $sqlNoti);
		$data = "Successfully Processed";
      	$errorm = null;
	}
      // $data = json_encode($row);
    }
  }else{
    $data = null;
    $errorm = "Try AGain!";
  }
  $myObj = new stdClass();
  $myObj->data = $data;
  $myObj->errorm = $errorm;
  $myJSON = json_encode($myObj);
  echo $myJSON;
}

if (isset($_POST['formName']) && $_POST['formName']=="updateAtheneumPartner"){

   	$partnerUniId =  $_POST['partnerUniId'];
   	$partnerName =  $_POST['partnerName'];
   	$phone =  $_POST['phone'];
   	$email =  $_POST['email'];
    $partnerCode =  $_POST['partnerCode'];
    $partnerCode = strtoupper($partnerCode);
    $partnerCode = preg_replace('/\s+/', '', $partnerCode);
    $commission = $_POST['commission'];
        
    // $data = $sessionDate;
   
   	$sql = "UPDATE ATHENEUM_PARTNERS SET `NAME`= '$partnerName', `EMAIL` = '$email', `PHONE` = '$phone', `PARTNER_ID` = '$partnerCode', `COMMISSION` = '$commission' WHERE UNI_ID = '$partnerUniId' ";
   	
   	$result = mysqli_query($link, $sql);
	
	if ($result) {

		$errorm = null;
	    $data = "Successfully Updated Partner";
	}else{
		$errorm = "Failed-> ".mysqli_error($link);
	}

   	// $data = $password;
   	
	$myObj = new stdClass();
	$myObj->data = $data;
	$myObj->errorm = $errorm;
	$myJSON = json_encode($myObj);
	echo $myJSON;

}

if (isset($_POST['formName']) && $_POST['formName']=="deleteAtheneumPartner"){
 	// $data = "Hello";
    $partnerId =  $_POST['partnerId'];
    
    // $data = $sessionDate;
   
   	$sql = "DELETE FROM ATHENEUM_PARTNERS WHERE UNI_ID='$partnerId'";
   	
   	$result = mysqli_query($link, $sql);
	
	if ($result) {

		$errorm = null;
	    $data = $userId;
	}else{
		$errorm = "Failed-> ".mysqli_error($link);
	}

   	// $data = $password;
   	
	$myObj = new stdClass();
	$myObj->data = $data;
	$myObj->errorm = $errorm;
	$myJSON = json_encode($myObj);
	echo $myJSON;

}

// SPARSH HOLIDAYS

// GET CENTRALLY DEFINED HOLIDAYS

if (isset($_GET['fetchCentralHolidays'])){

  $sql = "SELECT * FROM SPARSH_CENTRAL WHERE `ATTRIBUTE`= 'HOLIDAYS'";
  $result = mysqli_query($link, $sql);
  if ($result) {
    if(mysqli_num_rows($result)>0){
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      // $data = json_encode($row);
      $data = $row;
      $errorm = null;
    }else{
		$data = null;
		$errorm = "No Holidays Defined! Please Add Holidays!";
    }
  }else{
    $data = null;
    $errorm = "Try AGain!";
  }
  $myObj = new stdClass();
  $myObj->data = $data;
  $myObj->errorm = $errorm;
  $myJSON = json_encode($myObj);
  echo $myJSON;
}

// Save Holidays

if (isset($_POST['formName']) && $_POST['formName']=="setCentralWeeklyHolidays"){
 	// $data = "Hello";
    $weekDays =  $_POST['weekDays'];
    $holidays['weekdays']  = $weekDays;

    // $holidays = json_encode($holidays);
    
    $sql = "SELECT * FROM SPARSH_CENTRAL WHERE ATTRIBUTE = 'HOLIDAYS'";
    $result = mysqli_query($link, $sql);
 	
 	if ($result) {
	    if(mysqli_num_rows($result)>0){
	      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
	      // $data = json_encode($row);
	      $fetchedHolidays = $row['CONTENT_JSON'];
	      if ($fetchedHolidays) {
	      	$fetchedHolidays = json_decode($fetchedHolidays);
	      	if ($fetchedHolidays->otherdays) {
	      		$otherdays = $fetchedHolidays->otherdays;
	      		$holidays['otherdays']  = $otherdays;
	      	}
	      	$holidays = json_encode($holidays);
	      	$stmt = $link->prepare("UPDATE SPARSH_CENTRAL SET CONTENT_JSON = ? WHERE ATTRIBUTE = 'HOLIDAYS'");
	      	$stmt->bind_param("s", $holidays);
	      	if ($stmt->execute()) { 
	      		$data = "Success";
	    		$errorm = null;
	      	}

	      }
	    }else{
	    	$holidays = json_encode($holidays);
			$stmt = $link->prepare("INSERT INTO SPARSH_CENTRAL (`ATTRIBUTE`, `CONTENT_JSON`) VALUES ('HOLIDAYS', ?)");
	        $stmt->bind_param("s", $holidays);
	      	if ($stmt->execute()) { 
	      		$data = "Success";
	    		$errorm = null;
	      	}
	    }
	  }else{
	    $data = null;
	    $errorm = "Try AGain!";
	  }
   	
	$myObj = new stdClass();
	$myObj->data = $data;
	$myObj->errorm = $errorm;
	$myJSON = json_encode($myObj);
	echo $myJSON;

}

if (isset($_POST['formName']) && $_POST['formName']=="setCentralOtherHolidays"){
 	// $data = "Hello";
    $otherdays =  $_POST['otherHolidays'];
    // $holidays['otherdays']  = $otherdays;
    // $data = json_encode($otherdays);
    // $otherdays = (array) $otherdays;
    // foreach ($otherdays as $key => $value) {
    // 	$data =$key;
    // }

    $otherdays = json_decode($otherdays);
    $holidays['otherdays']  = $otherdays;

    // $holidays = json_encode($holidays);
    
    $sql = "SELECT * FROM SPARSH_CENTRAL WHERE ATTRIBUTE = 'HOLIDAYS'";
    $result = mysqli_query($link, $sql);
 	
 	if ($result) {
	    if(mysqli_num_rows($result)>0){
	      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
	      // $data = json_encode($row);
	      $fetchedHolidays = $row['CONTENT_JSON'];
	      if ($fetchedHolidays) {
	      	$fetchedHolidays = json_decode($fetchedHolidays);
	      	if ($fetchedHolidays->weekdays) {
	      		$weekdays = $fetchedHolidays->weekdays;
	      		$holidays['weekdays']  = $weekdays;
	      	}
	      	$holidays = json_encode($holidays);
	      	$stmt = $link->prepare("UPDATE SPARSH_CENTRAL SET CONTENT_JSON = ? WHERE ATTRIBUTE = 'HOLIDAYS'");
	      	$stmt->bind_param("s", $holidays);
	      	if ($stmt->execute()) { 
	      		$data = "Success";
	    		$errorm = null;
	      	}

	      }
	    }else{
	    	$holidays = json_encode($holidays);
			$stmt = $link->prepare("INSERT INTO SPARSH_CENTRAL (`ATTRIBUTE`, `CONTENT_JSON`) VALUES ('HOLIDAYS', ?)");
	        $stmt->bind_param("s", $holidays);
	      	if ($stmt->execute()) { 
	      		$data = "Success";
	    		$errorm = null;
	      	}
	    }
	  }else{
	    $data = null;
	    $errorm = "Try AGain!";
	  }
   	
	$myObj = new stdClass();
	$myObj->data = $data;
	$myObj->errorm = $errorm;
	$myJSON = json_encode($myObj);
	echo $myJSON;

}

//Content manage

function startsWith ($string, $startString) 
{ 
    $len = strlen($startString); 
    return (substr($string, 0, $len) === $startString); 
} 

	require_once("spaces.php");
	$space = new SpacesConnect(DO_KEY, DO_SECRET, DO_SPACE, DO_REGION);
	// $files = $space->ListObjects();
	$extension=array("jpeg","jpg","png");
	
	// UPLOAD IMAGES TO SERVER

	if (isset($_GET['uploadImages'])) {
		$class = $_POST['class'];
		$day = $_POST['day'];
		if (isset($_POST['videos'])) {
			$videos = $_POST['videos'];
			$videos = json_encode($videos);	
		}
		// Content Book
		if (!isset($_FILES["contentbookImg"])) {
			// $data = "Sorry";
		}else{
			$fileCounter = 0;
			//check for existing files in the folder
			for ($i=1; $i <50 ; $i++) { 
				if ($space->DoesObjectExist("ParentApp/".$class."/".$day."/CONTENTBOOK/".$i.".jpg")) {
					$fileCounter = $i;
				}
			}
			$fileCounter++;
			$contentBookImages = array();
			foreach($_FILES["contentbookImg"]["tmp_name"] as $key=>$tmp_name) {
				// $data .= $tmp_name;
				$file_name=$_FILES["contentbookImg"]["name"][$key];
			    $file_tmp=$_FILES["contentbookImg"]["tmp_name"][$key];
			    $ext=pathinfo($file_name,PATHINFO_EXTENSION);
			    if(in_array($ext,$extension)) {
			    	$fileName = "ParentApp/".$class."/".$day."/CONTENTBOOK/".$fileCounter.".jpg";
			    	if($space->UploadFile($file_tmp, "public", $fileName)){
			    	  array_push($contentBookImages, $fileName);
				      $data = "Content Book Uploaded Successfully";
				      $fileCounter++;
				    }else{
				      $data = "Could not Uploaded Content Book";
				    }
			    }else{
			    	$data = "Content Book should be an Image!";
			    }
			    
			}
			// FETCH DATABASE CONTENT
			$sql = "SELECT * FROM SPARSH_CONTENT WHERE CLASS = '$class' AND DAY = '$day'"; 
			$result = mysqli_query($link,$sql);
			if ($result) {
				if(mysqli_num_rows($result)>0){
					$row = mysqli_fetch_array($result);
					$recordId = $row['ID'];
					$prevContent = $row['CONTENT_BOOK'];
					if ($prevContent != null) {
						$prevContent = json_decode($prevContent, true);
						$contentBookImages = array_merge($prevContent, $contentBookImages);
					}
				}
			}
			$contentBookImages = json_encode($contentBookImages);
		}
		// WorkBook
		if (!isset($_FILES["workbookImg"])) {
			// $data = "Sorry";
		}else{
			$fileCounter = 0;
			for ($i=1; $i <50 ; $i++) { 
				if ($space->DoesObjectExist("ParentApp/".$class."/".$day."/WORKBOOK/".$i.".jpg")) {
					$fileCounter = $i;
				}
			}
			$fileCounter++;
			$workBookImages = array();
			foreach($_FILES["workbookImg"]["tmp_name"] as $key=>$tmp_name) {
				$file_name=$_FILES["workbookImg"]["name"][$key];
			    $file_tmp=$_FILES["workbookImg"]["tmp_name"][$key];
			    $ext=pathinfo($file_name,PATHINFO_EXTENSION);
			    if(in_array($ext,$extension)) {
			    	$fileName = "ParentApp/".$class."/".$day."/WORKBOOK/".$fileCounter.".jpg";
			    	if($space->UploadFile($file_tmp, "public", $fileName)){
			    	  array_push($workBookImages, $fileName);
				      $data .= "Work Book Uploaded Successfully";
				      $fileCounter++;
				    }else{
				      $data .= "Could not Uploaded Work Book";
				    }
			    }else{
			    	$data .= "Work Book should be an Image!";
			    }
			}
			// FETCH DATABASE CONTENT
			$sql = "SELECT * FROM SPARSH_CONTENT WHERE CLASS = '$class' AND DAY = '$day'"; 
			$result = mysqli_query($link,$sql);
			if ($result) {
				if(mysqli_num_rows($result)>0){
					$row = mysqli_fetch_array($result);
					$recordId = $row['ID'];
					$prevContent = $row['WORK_BOOK'];
					if ($prevContent != null) {
						$prevContent = json_decode($prevContent, true);
						$workBookImages = array_merge($prevContent, $workBookImages);
					}
				}
			}
			$workBookImages = json_encode($workBookImages);
		}
		//LessonPlan
		if (!isset($_FILES["lessonplanImg"])) {
			// $data = "Sorry";
		}else{
			$fileCounter = 0;
			for ($i=1; $i <50 ; $i++) { 
				if ($space->DoesObjectExist("ParentApp/".$class."/".$day."/LESSON_PLAN/".$i.".jpg")) {
					$fileCounter = $i;
				}
			}
			$fileCounter++;
			$lessonPlanImages = array();
			foreach($_FILES["lessonplanImg"]["tmp_name"] as $key=>$tmp_name) {
				$file_name=$_FILES["lessonplanImg"]["name"][$key];
			    $file_tmp=$_FILES["lessonplanImg"]["tmp_name"][$key];
			    $ext=pathinfo($file_name,PATHINFO_EXTENSION);
			    if(in_array($ext,$extension)) {
			    	$fileName = "ParentApp/".$class."/".$day."/LESSON_PLAN/".$fileCounter.".jpg";
			    	if($space->UploadFile($file_tmp, "public", $fileName)){
			    	  array_push($lessonPlanImages, $fileName);
				      $data .= "Lesson Plan Uploaded Successfully";
				      $fileCounter++;
				    }else{
				      $data .= "Could not Uploaded Lesson Plan";
				    }
			    }else{
			    	$data .= "Lesson Plan should be an Image!";
			    }
			}
			// FETCH DATABASE CONTENT
			$sql = "SELECT * FROM SPARSH_CONTENT WHERE CLASS = '$class' AND DAY = '$day'"; 
			$result = mysqli_query($link,$sql);
			if ($result) {
				if(mysqli_num_rows($result)>0){
					$row = mysqli_fetch_array($result);
					$recordId = $row['ID'];
					$prevContent = $row['WORK_BOOK'];
					if ($prevContent != null) {
						$prevContent = json_decode($prevContent, true);
						$lessonPlanImages = array_merge($prevContent, $lessonPlanImages);
					}
				}
			}
			$lessonPlanImages = json_encode($lessonPlanImages);
		}

		// DATABASE RECORD INSERT AND UPDATE

		$sql = "SELECT * FROM SPARSH_CONTENT WHERE CLASS = '$class' AND DAY = '$day'"; 
		$result = mysqli_query($link,$sql);
		if ($result) {
			if(mysqli_num_rows($result)>0){
				$sqlUpdate = "UPDATE SPARSH_CONTENT SET CONTENT_BOOK = '$contentBookImages', WORK_BOOK = '$workBookImages', LESSON_PLAN = '$lessonPlanImages' WHERE CLASS = '$class' AND DAY = '$day'";
				$resultUpdate = mysqli_query($link,$sqlUpdate);
				if ($resultUpdate) {
					$data = "Successfully Uploaded";
				}else{
					$data = "Could not upload";
				}
			}else{
				$sqlInsert = "INSERT INTO SPARSH_CONTENT (`CLASS`, `DAY`, `CONTENT_BOOK`, `WORK_BOOK`, `LESSON_PLAN`, `VIDEOS`)VALUES('$class', '$day', '$contentBookImages', '$workBookImages', '$lessonPlanImages', '$videos')";

				$resultInsert = mysqli_query($link,$sqlInsert);
				if (!$resultInsert) {
					$data = "Could not upload";
					$errorm = mysqli_error($link);
				}else{
					$data = "Successfully Uploaded";
				}

			}
		}else{
			$errorm = mysqli_error($link);
		}

		$myObj = new stdClass();
		$myObj->result = $data;
		$myObj->errorm = $errorm;
		$myJSON = json_encode($myObj);
		echo $myJSON; 
		
	}

	//FETCH CONTENT FOR THE CLASS

	if (isset($_GET['fetchContent'])){
		$className = $_GET['class'];
		$sql = "SELECT * FROM SPARSH_CONTENT WHERE CLASS = '$className' ORDER BY DAY ASC";
	     $result = mysqli_query($link,$sql);
	     $tempArray = [];
	     if ($result) {
	        if(mysqli_num_rows($result)>0){
	          while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
	            array_push($tempArray, $row);
	          }
	          $data = json_encode($tempArray);
	        }else{
	          // $errorm = "No blog avaliable. Please create one!";
	          // $errorm =json_encode($errorm);
	          $data = null;
	          $data = json_encode($data);
	        }
	      }else{
	        $errorm = mysqli_error($link);
	      }
		$myObj = new stdClass();
		$myObj->result = $data;
		$myObj->errorm = $errorm;
		$myJSON = json_encode($myObj);
		echo $myJSON; 
	}

	//DELETE CONTENT

	if (isset($_GET['deleteFile'])){
		$fileName = $_POST['fileName'];
		if ($space->DoesObjectExist($fileName)) {
			if ($space->DeleteObject($fileName)) {
				$result = "Successfully Deleted File";
			}else{
				$result = "Could Not Delete File";
			}
		}else{
			$result = "Could Not Delete File";
		}
		$myObj = new stdClass();
		$myObj->result = $result;
		// $myObj->errorm = $errorm;
		$myJSON = json_encode($myObj);
		echo $myJSON; 
	}

	//Get videos list

	if (isset($_GET['getVideosList'])){
		$files = $space->ListObjects();
		$fileLists = array();

		foreach ($files as $key => $value) {
		    // echo $value['Key']."";
		    // echo $value['LastModified']."<br>";
		    if (startsWith($value['Key'], "TB/WB-AV-Mapping-Work/")) {
		    	array_push($fileLists,$value['Key']);
		    }
		}

		$myObj = new stdClass();
		$myObj->files = $fileLists;
		// $myObj->errorm = $errorm;
		$myJSON = json_encode($myObj);
		echo $myJSON; 
	}

	//Check if the day already exist in the db

	if (isset($_GET['checkDayExist'])){
		$day = $_GET['day'];
		$class = $_GET['class'];

		$sql = "SELECT * FROM SPARSH_CONTENT WHERE DAY = '$day' AND CLASS='$class'";
		$result = mysqli_query($link,$sql);
		if ($result) {
			if(mysqli_num_rows($result)>0){
				$dayExist = 'true';
			}else{
				$dayExist = 'false';
			}	
		}else{
			$dayExist = mysqli_error($link);
		}
		
		$myObj = new stdClass();
		$myObj->dayExist = $dayExist;
		// $myObj->errorm = $errorm;
		$myJSON = json_encode($myObj);
		echo $myJSON; 
	}


	// GET DAYS CIONTENT

	if (isset($_GET['getContentForTheDay'])){
		$day = $_GET['day'];
		$class = $_GET['class'];
		// $data = "sjdhf";

		// CHECK FOR FILE EXISTENCE

		$contentBookPresent = array();
		$workBookPresent = array();
		$lessonPlanPresent = array();

		//--CHECK CONTENT BOOK--

		for ($i=1; $i <30 ; $i++) { 
			if ($space->DoesObjectExist("ParentApp/".$class."/".$day."/CONTENTBOOK/".$day."-".$i.".jpg")) {
				array_push($contentBookPresent,"ParentApp/".$class."/".$day."/CONTENTBOOK/".$day."-".$i.".jpg");
			}else{
				
			}
		}

		if (count($contentBookPresent) == 0) {
			$contentBookPresent = null;
		}


		//--CHECK WORK BOOK--

		for ($i=1; $i <30 ; $i++) { 
			if ($space->DoesObjectExist("ParentApp/".$class."/".$day."/WORKBOOK/".$day."-".$i.".jpg")) {
				array_push($workBookPresent,"ParentApp/".$class."/".$day."/WORKBOOK/".$day."-".$i.".jpg");
			}else{
				break;
			}
		}

		if (count($workBookPresent) == 0) {
			$workBookPresent = null;
		}

		//--CHECK LESSON PLAN--


		for ($i=1; $i <30 ; $i++) { 
			if ($space->DoesObjectExist("ParentApp/".$class."/".$day."/LESSON_PLAN/".$day."-".$i.".jpg")) {
				array_push($lessonPlanPresent,"ParentApp/".$class."/".$day."/LESSON_PLAN/".$day."-".$i.".jpg");
			}else{
				break;
			}
		}

		if (count($lessonPlanPresent) == 0) {
			$lessonPlanPresent = null;
		}

		$myObj = new stdClass();
		$myObj->contentBook = $contentBookPresent;
		$myObj->workBook = $workBookPresent;
		$myObj->lessonPlan = $lessonPlanPresent;
		// $myObj->errorm = $errorm;
		$myJSON = json_encode($myObj);
		echo $myJSON;

	}

}

?>



<?php 

// payment confirmation

if (isset($_GET['confirmPayment'])) {
	$msg = null;
	$error = null;
	$userId = $_POST['user'];
	$sql = "SELECT * FROM ATHENEUM_STUDENT WHERE UNI_ID = '$userId'";
	$result = mysqli_query($link, $sql);
	if($result){
		$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
		$programType = $row["PROGRAM_TYPE"];
        $programName = $row["PROGRAM_NAME"];
        $status = $row["STATUS"];
        $referals = $row["REFERALS"];
        $referredBy = $row["REFERRED_BY"];
        $earning = $row["EARNING"];
        $paid = $row["PAID"];
        $partnerId = $row["PARTNER_ID"];
        $jsonDetails = $row["DETAILS"];
        $jsonDetails = json_decode($jsonDetails);
        $country = $jsonDetails->{'country'};
        $paidMoney = 0;
        $refMoney = 0;
        if ($country == "India") {
        	if ($programType == "Certification Course") {
	        	$paidMoney = 9500;
	        	$refMoney = 500;
	        }else if($programType == "Graduate Course"){
	        	$paidMoney = 15500;
	        	$refMoney = 750;
	        }else if($programType == "Post Graduate Course"){
	        	$paidMoney = 25500;
	        	$refMoney = 1000;
	        }else{
	        	$error = "Not Enrolled for valid Course";
	        }
        }else{
        	if ($programType == "Certification Course") {
	        	$paidMoney = 150;
	        	$refMoney = 10;
	        }else if($programType == "Graduate Course"){
	        	$paidMoney = 250;
	        	$refMoney = 15;
	        }else if($programType == "Post Graduate Course"){
	        	$paidMoney = 400;
	        	$refMoney = 20;
	        }else{
	        	$error .= "Not Enrolled for valid Course";
	        }	
        }
        if ($paidMoney != 0) {
        	$sqlUpdate = "UPDATE ATHENEUM_STUDENT SET PAID = '$paidMoney' WHERE UNI_ID = '$userId'";
        	$resultUpdate = mysqli_query($link, $sqlUpdate);
        	if($resultUpdate){
        		// Giving partner commission
        		if ($partnerId) {
        			$sqlFetchPartner = "SELECT * FROM ATHENEUM_PARTNERS WHERE PARTNER_ID = '$partnerId'";
        			$resultFetchPartner = mysqli_query($link, $sqlFetchPartner);
        			if ($resultFetchPartner) {
        				$rowPartner = mysqli_fetch_array($resultFetchPartner,MYSQLI_ASSOC);
        				$walletMoney = $rowPartner['WALLET_MONEY'];
        				$commissionPer = $rowPartner['COMMISSION'];
        				$partnerEmail = $rowPartner['EMAIL'];
        				$commissionPer = (int) $commissionPer;
        				if ($walletMoney) {
        					$walletMoney = json_decode($walletMoney);
        					$indianMoney = $walletMoney->inr;
        					if ($indianMoney) {
        						$indianMoney = (int) $indianMoney;
        					}else{
        						$indianMoney = 0;
        					}
        					$dollarMoney = $walletMoney->dollar;
        					if ($dollarMoney) {
        						$dollarMoney = (int) $dollarMoney;
        					}else{
        						$dollarMoney = 0;
        					}
        					$commissionMoney = ($paidMoney*$commissionPer)/100;
        					if ($country == "India") {
        						$indianMoney = $commissionMoney+$indianMoney;
        					}else{
        						$dollarMoney = $commissionMoney+$dollarMoney;
        					}
        					$partnerCom = array('inr' => $indianMoney, 'dollar' => $dollarMoney);
        				}else{
        					$commissionMoney = ($paidMoney*$commissionPer)/100;
        					if ($country == "India") {
        						$partnerCom = array('inr' => $commissionMoney, 'dollar' => 0);
        					}else{
        						$partnerCom = array('inr' => 0, 'dollar' => $commissionMoney);
        					}
        				}
        				$partnerCom = json_encode($partnerCom);
        				$sqlUpdatePartner = "UPDATE ATHENEUM_PARTNERS SET WALLET_MONEY = '$partnerCom' WHERE PARTNER_ID = '$partnerId'";
        				$resultUpdatePartner = mysqli_query($link, $sqlUpdatePartner);
        				if ($resultUpdatePartner) {

       //  					echo '<script>sendMail(`'.$partnerEmail.'`, `'.$subject.'`, `'.$msgToSend.'`)</script>';
							// echo '<script>sendMail("chatterjeegouravking@gmail.com", `'.$msgToSend.'`)</script>';	

							$sqlNoti = "INSERT INTO ATHENEUM_NOTIFICATION (`USER_ID`, `USER_TYPE`, `TITLE`, `MESSAGE`, `LINK`, `STATUS`) VALUES ('$partnerId', 'PARTNER', 'RECEIVED MONEY IN WALLET', 'You have received money in wallet', 'rewards', 'PENDING')";
        					$resultNoti = mysqli_query($link, $sqlNoti);
        				}

        			}
        		}

        		if ($referredBy != null) {
        			$sqlFindRef = "SELECT * FROM ATHENEUM_STUDENT WHERE UNI_ID = '$referredBy'";
        			$resultFindRef = mysqli_query($link, $sqlFindRef);
        			$row = mysqli_fetch_array($resultFindRef,MYSQLI_ASSOC);	
        			$earnedMoney = $row['EARNING'];
        			$updatedEarnedMoney = $earnedMoney + $refMoney;
        			$sqlUpdateRef = "UPDATE ATHENEUM_STUDENT SET EARNING = '$updatedEarnedMoney' WHERE UNI_ID = '$referredBy'";
        			$resultUpdatedRef = mysqli_query($link, $sqlUpdateRef);
        			if ($resultUpdatedRef) {
        				$msg .= "Succesfully confirmed the payment"; 
		        		$params = array(
				         "sendMail" => true,
				         "reciever" => $referredBy,
				         "sender" => "Atheneum Global",
				         "senderMail" => "noreply@atheneumglobal.com",
				         "subject" => "Reward from Atheneum Global",
				         "message" => "<html>
				                        <head>
				                        <title>Welcome To Athemeum Global College</title>
				                        </head>
				                        <body>
				                        <h2>Welcome to Atheneum Global Teacher Traing College </h2>
				                        <h3>Hey, You just have earnt a reward in your wallet. One of your referals have signed up for our course today and we are happy to give you the credit for that. Keep referring to earn more bonus. <a href='https://user.atheneumglobal.education'>Click </a>here to see details.</h3>
				                        <hr>
				                        <h3><a href='https://atheneumglobal.education/enroll'>Click here to see our courses.</a></h3>
				                        <h3>Hey you can earn money by just referring to your friends or family members. Tell them to join Atheneum Global Teacher Training College now.<a href='https://user.athenemglobal.education'>here</a>.</h3>
				                        <h4>We're in the business of breaking down barriers. Find out how we make practical, career-focused learning more accessible than ever before.</h4>
				                        </body>
				                        </html>"
				        );
				        $response = httpPost("https://api.teenybeans.in/API/mailApi/v1/",$params);
        			}

        		}

        		$sql = "INSERT INTO ATHENEUM_NOTIFICATION (`USER_ID`, `USER_TYPE`, `TITLE`, `MESSAGE`, `LINK`, `STATUS`) VALUES ('$userId', 'STUDENT', 'Payment Approved', 'Your payment has been accepted by the admin and you will be given the access of our course', '/', 'PENDING')";
        		$result = mysqli_query($link, $sql);
        	}else{
        		$error = "Some Error".mysqli_error($link);
        	}
        }else{
        	$error = "Not enrolled in any course";
        }
	}else{
		echo '<div class="alert alert-danger">Error Running the Query</div>';
		echo '<div class="alert alert-danger">' . mysqli_error($link) . '</div>';
	}
	  $myObj = new stdClass();
	  $myObj->msg = $msg;
	  $myObj->error = $error;
	  $myJSON = json_encode($myObj);
	  echo $myJSON;

}



 ?>