<?php 
if (isset($_FILES['files'])) {

	// CREATE A DIRECTORY WHERE YOU WANT TO STORE THE FILES AND GIVE PERMISSION
    mkdir("Files/uploadedFiles/",  0755, true);

    foreach($_FILES['files']['name'] as $key=>$val){ // RUN A LOOP THROUGH THE FILES ARRAY
        $filename = basename($_FILES['files']['name'][$key]);

        // REMOVE SPACES AND OTHER CHARACTERS FROM FILE NAME
        $filename = preg_replace("![^a-z0-9.]+!i", "-", $filename); 
        
        // SET THE UPLOAD PATH
        $target = "Files/uploadedFiles/".$filename;

        // UPLOAD THE FILES
	    if (move_uploaded_file($_FILES['files']['tmp_name'][$key], $target)) {
	        echo "File uploaded successfully";
	    }else{
	        echo "Failed to upload ".$filename;
	    }
    }
}
?>

<div class="container">
	<div class="shadow card col-6 mx-auto">
		<div class="card-body text-center">
			<img src="/IMAGES/server.png" width="80" height="80">
			<h2>Upload Files to server using PHP</h2><hr>
			<form id="uploadForm" method="POST" enctype="multipart/form-data">
	            <input id="file" type="file" style="display: none;" name="files[]" multiple 
	             onchange="form.submit()" />
	         </form>
            <a href="#" class="btn btn-primary" onclick="selectImages()" style="text-decoration:none;"><i class="fa fa-upload"></i> Upload Files</a>
		</div>
	</div>
</div>

<script type="text/javascript">
	function selectImages() {
		document.getElementById('file').click();
	}
</script>